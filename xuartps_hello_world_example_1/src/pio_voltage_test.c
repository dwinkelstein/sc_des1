/*
 * pio_voltage_test.c
 *
 *  Created on: Mar 18, 2021
 *      Author: DanWinkelstein
 */

#include "SC_des1.h"

extern SC_DES1_T sc_des1_inst;   //out global register definition file
void contactor_toggle();
int copyRealTimeData();


void pio_voltage() {
	CONTROL_REG_T local_control_reg;
	local_control_reg.reg = 0;
	local_control_reg.bits.dataCollectEnable = 1;
	local_control_reg.bits.greenLEDEnable = 1;
	local_control_reg.bits.interruptEnable = 0;
	local_control_reg.bits.streamIntEnable = 0;
	local_control_reg.bits.freq60_50 = F60HZ;
	set_control_reg(&sc_des1_inst, &local_control_reg);

	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	sc_des1_inst.alarm_data->keypad_value_reg.bits.key1Value = 1;
	int breakContactor;
	breakContactor = 0;

	while(1){
		sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
		sc_des1_inst.alarm_data->keypad_value_reg.bits.key1Value = 1;  //DISPLAY VOLTAGE
		get_snapshot_data(&sc_des1_inst);
		get_temperature(&sc_des1_inst);
		get_alarm_data(&sc_des1_inst);
		debugPrintSnapshot(&sc_des1_inst);
		standalone_display(&sc_des1_inst);
		get_contactor_reg(&sc_des1_inst);
		get_circuitbreaker_reg(&sc_des1_inst);
		sleep(5);
		sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
		sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
		standalone_display(&sc_des1_inst);
		sleep(1);
		sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
		sc_des1_inst.alarm_data->keypad_value_reg.bits.key7Value = 1;  //DISPLAY TEMPERATURE
		standalone_display(&sc_des1_inst);
		sleep(5);
		sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
		sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
		standalone_display(&sc_des1_inst);
		sleep(1);
		sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
		sc_des1_inst.alarm_data->keypad_value_reg.bits.key3Value = 1;  //DISPLAY FREQ/PHASE
		standalone_display(&sc_des1_inst);
		sleep(5);
		sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
		sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
		standalone_display(&sc_des1_inst);
		sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
		sc_des1_inst.alarm_data->keypad_value_reg.bits.key2Value = 1;
		standalone_display(&sc_des1_inst);
		sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
		sc_des1_inst.alarm_data->keypad_value_reg.bits.key3Value = 1;  //MAIN 60B
		standalone_display(&sc_des1_inst);
		sleep(5);
		sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
		sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
		standalone_display(&sc_des1_inst);
		sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
		sc_des1_inst.alarm_data->keypad_value_reg.bits.key4Value = 1;  //Alarms
		standalone_display(&sc_des1_inst);
		sleep(5);
		sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
		sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
		standalone_display(&sc_des1_inst);
		sleep(1);
		breakContactor += 1;
		if(breakContactor == 5) {
			breakContactor = 0;
			contactor_toggle();
		}
	}
}

void contactor_toggle(){
	CONTACTOR_DISENGAGE_T cycle_contactors;
		if(sc_des1_inst.alarm_data->contactor_disengage_reg.reg != 0) {
			cycle_contactors.reg = 0;
		} else {
			cycle_contactors.bits.bypassContactor = 1;
			cycle_contactors.bits.f20AContactor = 1;
			cycle_contactors.bits.f20BContactor = 1;
			cycle_contactors.bits.f40AContactor = 1;
			cycle_contactors.bits.f40BContactor = 1;
			cycle_contactors.bits.f60AContactor = 1;
			cycle_contactors.bits.f60BContactor = 1;
		}
		set_contactor(cycle_contactors, &sc_des1_inst);
		sleep(1);
}

void xdma_test() {
	// hit start
	CONTROL_REG_T local_control_reg;
	local_control_reg.reg = 0;
	local_control_reg.bits.dataCollectEnable = 1;
	local_control_reg.bits.greenLEDEnable = 1;
	local_control_reg.bits.interruptEnable = 0;
	local_control_reg.bits.streamIntEnable = 0;
	local_control_reg.bits.alarmInterruptEnable = 0;
	local_control_reg.bits.snapShotIntEnable = 0;
	local_control_reg.bits.freq60_50 = F60HZ;
	set_control_reg(&sc_des1_inst, &local_control_reg);

	//enable interrupts in the AXIS/AXI fifo
//	Xil_Out32()
	Xil_Out32(FIFO_REG_IER_ADDR, 0x00100000); // Set the IER for Recieve Fifo Programmable Full (PG080 page 22)
	Xil_Out32(FIFO_REG_ISR_ADDR, 0xFFF80000); // Clear the ISR register (PG080 page 22)
	local_control_reg.bits.streamCollectEnable = 1;
	set_control_reg(&sc_des1_inst, &local_control_reg);

	int totalCollectedData = 0;
	while(1){

		totalCollectedData += copyRealTimeData();
		usleep(400);
	}
}
