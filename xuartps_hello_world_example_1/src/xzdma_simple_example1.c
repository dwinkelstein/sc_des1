/******************************************************************************
* Copyright (C) 2014 - 2020 Xilinx, Inc.  All rights reserved.
* SPDX-License-Identifier: MIT
******************************************************************************/

/*****************************************************************************/
/**
*
* @file xzdma_simple_example.c
*
* This file contains the example using XZDma driver to do simple data transfer
* in Normal mode on ZDMA device for 1MB data transfer.
*
* <pre>
* MODIFICATION HISTORY:
*
* Ver   Who     Date     Changes
* ----- ------  -------- ------------------------------------------------------
* 1.0   vns     2/27/15  First release
*       vns    10/13/15  Declared static array rather than hard code memory.
*       ms     04/05/17  Modified comment lines notation in functions to
*                        avoid unnecessary description to get displayed
*                        while generating doxygen.
* 1.3   mus    08/14/17  Do not perform cache operations if CCI is enabled
* 1.4   adk    11/02/17  Updated example to fix compilation errors for IAR
*			 compiler.
* 1.5   adk    11/22/17  Added peripheral test app support.
*		12/11/17 Fixed peripheral test app generation issues when dma
*			 buffers are configured on OCM memory(CR#990806).
*		18/01/18 Remove unnecessary column in XIntc_Connect() API.
*		01/02/18 Added support for error handling.
* 1.7   adk    21/03/19  Fix alignment pragmas in the example for IAR compiler.
*	       19/04/19  Rename the dma buffers to avoid peripheral
*			 test compilation errors with armclang compiler.
* </pre>
*
******************************************************************************/

/***************************** Include Files *********************************/

#include "xzdma.h"
#include "xparameters.h"
#include "xil_cache.h"
#include "SC_des1.h"
#include <math.h>
#include "xscugic.h"

/************************** Constant Definitions ******************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define ZDMA_DEVICE_ID		XPAR_XZDMA_0_DEVICE_ID /* ZDMA device Id */
#define INTC		XScuGic
#define ZDMA_INTC_DEVICE_ID	XPAR_SCUGIC_SINGLE_DEVICE_ID
					/**< SCUGIC Device ID */
#define ZDMA_INTR_DEVICE_ID	XPAR_XADMAPS_0_INTR /**< ZDMA Interrupt Id */

#define TRANSFERSIZE		256    /**< Size of the data to be transferred per burst in words */
//#define BUFFERSIZE		1048576     /**< Size of the buffer in words */ // this falls of the end of the stack
#define BUFFERSIZE		65536     /**< Size of the buffer in words */

#define TESTVALUE	0xDEADBEEF /**< For writing into source buffer */

/**************************** Type Definitions *******************************/

/************************** Function Prototypes ******************************/
u32 ZDmaDstBuf[BUFFERSIZE]  __attribute__ ((aligned (64)));	/**< Destination buffer */

//int XZDma_SimpleExample(INTC *IntcInstPtr, XZDma *ZdmaInstPtr,
//			u16 DeviceId, u16 IntrId);
//int XDMA_Tranfer_example(XZDma *ZdmaInstPtr, u32 *BufferPtr, int startIndex);
static int SetupInterruptSystem_example(INTC *IntcInstancePtr,
				XZDma *InstancePtr, u16 IntrId);
static void DoneHandler(void *CallBackRef);
static void ErrorHandler(void *CallBackRef, u32 Mask);
void MemoryCopy();
void clear_LCD();
int LCD_output(char write_buffer[], int buffer_size);


/************************** Variable Definitions *****************************/

XZDma ZDma;		/**<Instance of the ZDMA Device */
static INTC XZDmaIntc;	/**< XIntc Instance */

volatile u32 AlloMem[200]  __attribute__ ((aligned (64)));	/**< memory allocated for descriptors */

volatile static int Done = 0;				/**< Variable for Done interrupt */
volatile static int Pause = 0;			    /**< Variable for Pause interrupt*/

// this is the complete register map for the hardware
SC_DES1_T sc_des1_inst;   //out global register definition file
XZDma_Config *Config;
XZDma_DataConfig Configure; /* Configuration values */
SNAPSHOT_MEMORY_T snapshot_data;
ALARM_REGISTER_T alarm_data;
SENSOR_FACTOR_T sensor_factor;
PCA9539_T pca9539;

ALARM_LIMIT_T alarm_limit_value; // this holds the alarm limits we plan to install

CONTACTOR_DISENGAGE_T contactor_setting;

/*****************************************************************************/
/**
*
* Main function to call the example.
*
* @return
*		- XST_SUCCESS if successful.
*		- XST_FAILURE if failed.
*
* @note		None.
*
******************************************************************************/

int dma_test();

#ifdef DMATEST
int main(){
	dma_test();
}
#endif

int externalDMAInit(){
	int Status;
	Status = XZDma_SimpleExample(&XZDmaIntc, &ZDma, (u16)ZDMA_DEVICE_ID,
				     ZDMA_INTR_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("ZDMA Simple Example SETUP Failed\r\n");
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

int dma_test()
{
	int Status;

	sc_des1_inst.snapshot_data = &snapshot_data;
	sc_des1_inst.alarm_data = &alarm_data;
	sc_des1_inst.sensor_factor = &sensor_factor;
	sc_des1_inst.temperature = 0;
	sc_des1_inst.pca9539 = &pca9539;

	CONTROL_REG_T local_control_reg;
	local_control_reg.reg = 0;
	set_control_reg(&sc_des1_inst, &local_control_reg);
	sleep(1);
	local_control_reg.bits.dataCollectEnable = 1;
	local_control_reg.bits.greenLEDEnable = 1;
	local_control_reg.bits.interruptEnable = 0;
	local_control_reg.bits.streamIntEnable = 0;
	local_control_reg.bits.alarmInterruptEnable = 0;
	local_control_reg.bits.snapShotIntEnable = 0;
	local_control_reg.bits.freq60_50 = F60HZ;
	set_control_reg(&sc_des1_inst, &local_control_reg);
#ifndef IPC_DISPLAY
	// initialize LCD display UART
	UartPsLCDinit(LCD_UART_DEVICE_ID);
#endif

	clear_LCD();
	LCD_output("XDMA Test 7",11);
	set_timestamp(&sc_des1_inst);
	/* Filling the buffer for data transfer */
	for (int Index = 0; Index < BUFFERSIZE; Index++) {
		ZDmaDstBuf[Index] = Index;
	}
	/* Run the simple example */
	Status = XZDma_SimpleExample(&XZDmaIntc, &ZDma, (u16)ZDMA_DEVICE_ID,
				     ZDMA_INTR_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("ZDMA Simple Example SETUP Failed\r\n");
		return XST_FAILURE;
	}

	//enable interrupts in the AXIS/AXI fifo
//	Xil_Out32()
	Xil_Out32(FIFO_REG_IER_ADDR, 0x00100000); // Set the IER for Recieve Fifo Programmable Full (PG080 page 22)
	Xil_Out32(FIFO_REG_ISR_ADDR, 0xFFF80000); // Clear the ISR register (PG080 page 22)
	local_control_reg.bits.streamCollectEnable = 1;
	set_control_reg(&sc_des1_inst, &local_control_reg);
//	usleep(1000);
//	local_control_reg.bits.streamCollectEnable = 0;
//	set_control_reg(&sc_des1_inst, &local_control_reg);
//	LCD_output("Memory copy ",11);
//
//	MemoryCopy();
//	return 0;




	int wordsInFifo_pre;
	int wordsInFifo_post;

	//Fill buffer up with 1 second of data
	int wordsTransfered = 0;
	while(wordsTransfered < BUFFERSIZE) {
		wordsInFifo_pre = Xil_In32(FIFO_REG_RDFO_ADDR);
		while(wordsInFifo_pre < 2*TRANSFERSIZE) {
			wordsInFifo_pre = Xil_In32(FIFO_REG_RDFO_ADDR);
		}
    	wordsTransfered +=  XDMA_Tranfer_example(&ZDma,&ZDmaDstBuf[0],wordsTransfered);
	}
	wordsInFifo_post = Xil_In32(FIFO_REG_RDFO_ADDR);

	// Discard data and fill buffer again
	wordsTransfered = 0;
	while(wordsTransfered < BUFFERSIZE) {
		wordsInFifo_pre = Xil_In32(FIFO_REG_RDFO_ADDR);
		while(wordsInFifo_pre < 2*TRANSFERSIZE) {
			wordsInFifo_pre = Xil_In32(FIFO_REG_RDFO_ADDR);
		}
		wordsTransfered +=  XDMA_Tranfer_example(&ZDma,&ZDmaDstBuf[0],wordsTransfered);
	}
	wordsInFifo_post = Xil_In32(FIFO_REG_RDFO_ADDR);

	printf("\r\n Words transfered %d words in FIFO pre %d words in FIFO post %d\r\n",wordsTransfered, wordsInFifo_pre,wordsInFifo_post);

	for (int Index = 0; Index < wordsTransfered; Index++) {
		printf("%08x \r\n",ZDmaDstBuf[Index]);
	}

	xil_printf("Successfully ran ZDMA Simple Example\r\n");
	return XST_SUCCESS;
}

/*****************************************************************************/
/**
*
* This function does a test of the data transfer in simple mode of normal mode
* on the ZDMA driver.
*
* @param	DeviceId is the XPAR_<ZDMA Instance>_DEVICE_ID value from
*		xparameters.h.
*
* @return
*		- XST_SUCCESS if successful.
*		- XST_FAILURE if failed.
*
* @note		None.
*
******************************************************************************/
int XZDma_SimpleExample(INTC *IntcInstPtr, XZDma *ZdmaInstPtr,
			u16 DeviceId, u16 IntrId)
{
	int Status;
	/*
	 * Initialize the ZDMA driver so that it's ready to use.
	 * Look up the configuration in the config table,
	 * then initialize it.
	 */
	Config = XZDma_LookupConfig(DeviceId);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XZDma_CfgInitialize(ZdmaInstPtr, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	/*
	 * Performs the self-test to check hardware build.
	 */
	Status = XZDma_SelfTest(ZdmaInstPtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}


//	/*
//	 * Invalidating destination address and flushing
//	 * source address in cache
//	 */
//	if (!Config->IsCacheCoherent) {
//		Xil_DCacheFlushRange((INTPTR)ZDmaDstBuf, BUFFERSIZE);
//		Xil_DCacheInvalidateRange((INTPTR)ZDmaDstBuf, BUFFERSIZE);
//	}

	/* ZDMA has set in simple transfer of Normal mode */
	Status = XZDma_SetMode(ZdmaInstPtr, TRUE, XZDMA_NORMAL_MODE);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/* Allocated memory starting address should be 64 bit aligned */
	XZDma_CreateBDList(&ZDma, XZDMA_LINEAR, (UINTPTR)AlloMem, 256);

	/* Interrupt call back has been set */
	XZDma_SetCallBack(ZdmaInstPtr, XZDMA_HANDLER_DONE,
				(void *)DoneHandler, ZdmaInstPtr);
	XZDma_SetCallBack(ZdmaInstPtr, XZDMA_HANDLER_ERROR,
				(void *)ErrorHandler, ZdmaInstPtr);
	/*
	 * Connect to the interrupt controller.
	 */
	Status = SetupInterruptSystem_example(IntcInstPtr, ZdmaInstPtr,
				      IntrId);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;

}

int XDMA_Tranfer_example(XZDma *ZdmaInstPtr, u32 *ZDmaDstBuf, int startIndex){
	XZDma_Transfer Data[2];
//	int Index;

	/* Configuration settings */
	Configure.OverFetch = 0;
	Configure.SrcIssue = 0x1F;
	Configure.SrcBurstType = XZDMA_FIXED_BURST;
	Configure.SrcBurstLen = 0xF; //256 words
	Configure.DstBurstType = XZDMA_INCR_BURST;
	Configure.DstBurstLen = 0xF;  //256 words
	Configure.SrcCache = 0x2;
	Configure.DstCache = 0x2;
	if (Config->IsCacheCoherent) {
		Configure.SrcCache = 0xF;
		Configure.DstCache = 0xF;
	}
//	Configure.SrcQos = 0;
//	Configure.DstQos = 0;

	XZDma_SetChDataConfig(ZdmaInstPtr, &Configure);

	/* Enable required interrupts */
	XZDma_EnableIntr(ZdmaInstPtr, (XZDMA_IXR_DMA_DONE_MASK | XZDMA_IXR_DMA_PAUSE_MASK));

	/*
	 * Transfer elements
	 */
	int wordsTransfered = 0;
//	wordsInFifo_pre = Xil_In32(FIFO_REG_RDFO_ADDR);

		Data[0].DstAddr = (UINTPTR)&ZDmaDstBuf[startIndex];
		Data[0].Size = TRANSFERSIZE*4; /* Size in bytes */
		Data[0].SrcAddr = FIFO_STREAM_ADDR;
		Data[0].Pause = 1;
		if (!Config->IsCacheCoherent) {
			Data[0].DstCoherent = 1;
			Data[0].SrcCoherent = 1;
		} else {
			Data[0].DstCoherent = 0;
			Data[0].SrcCoherent = 0;
		}

		Data[1].DstAddr = (UINTPTR)&ZDmaDstBuf[startIndex + TRANSFERSIZE];
		Data[1].Size = TRANSFERSIZE*4; /* Size in bytes */
		Data[1].SrcAddr = FIFO_STREAM_ADDR;
		Data[1].Pause = 0;
		if (!Config->IsCacheCoherent) {
			Data[1].DstCoherent = 1;
			Data[1].SrcCoherent = 1;
		} else {
			Data[1].DstCoherent = 0;
			Data[1].SrcCoherent = 0;
		}


		/*
		 * Invalidating destination address and flushing
		 * source address in cache
		 */
		if (!Config->IsCacheCoherent) {
			Xil_DCacheFlushRange((INTPTR)Data[0].SrcAddr, Data[0].Size);
			Xil_DCacheInvalidateRange((INTPTR)Data[0].DstAddr, Data[0].Size);
			Xil_DCacheFlushRange((INTPTR)Data[1].SrcAddr, Data[1].Size);
			Xil_DCacheInvalidateRange((INTPTR)Data[1].DstAddr, Data[1].Size);
		}

		XZDma_Start(ZdmaInstPtr, Data, 2); /* Initiates the data transfer */

		/* wait until pause interrupt is generated and has been resumed */
		while (Pause == 0);  // transfer 0
  		Pause = 0;
		/* Resuming the ZDMA core */
		XZDma_Resume(ZdmaInstPtr);
		/* Wait till DMA error or done interrupt generated */
		while (Done == 0);  // Transfer 1
		Done = 0;

		wordsTransfered = (Data[0].Size + Data[1].Size)/4;
//	    wordsInFifo_post = Xil_In32(FIFO_REG_RDFO_ADDR);
//		printf("\r\n Words transfered %d words in FIFO pre %d words in FIFO post %d\r\n",wordsTransfered, wordsInFifo_pre,wordsInFifo_post);
//
//		for (Index = 0; Index < wordsTransfered; Index++) {
//			printf("%08x \r\n",ZDmaDstBuf[Index]);
//		}
		XZDma_DisableIntr(ZdmaInstPtr, XZDMA_IXR_ALL_INTR_MASK);

	return wordsTransfered;
}

/*****************************************************************************/
/**
* This function sets up the interrupt system so interrupts can occur for the
* ZDMA. This function is application-specific. The user should modify this
* function to fit the application.
*
* @param	IntcInstancePtr is a pointer to the instance of the INTC.
* @param	InstancePtr contains a pointer to the instance of the ZDMA
*		driver which is going to be connected to the interrupt
*		controller.
* @param	IntrId is the interrupt Id and is typically
*		XPAR_<ZDMA_instance>_INTR value from xparameters.h.
*
* @return
*		- XST_SUCCESS if successful
*		- XST_FAILURE if failed
*
* @note		None.
*
****************************************************************************/
int SetupInterruptSystem_example(INTC *IntcInstancePtr,
				XZDma *InstancePtr,
				u16 IntrId)
{
	int Status;

	XScuGic_Config *IntcConfig; /* Config for interrupt controller */

	/*
	 * Initialize the interrupt controller driver
	 */
	IntcConfig = XScuGic_LookupConfig(ZDMA_INTC_DEVICE_ID);
	if (NULL == IntcConfig) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
					IntcConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Connect the interrupt controller interrupt handler to the
	 * hardware interrupt handling logic in the processor.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(Xil_ExceptionHandler) XScuGic_InterruptHandler,
				IntcInstancePtr);

	/*
	 * Connect a device driver handler that will be called when an
	 * interrupt for the device occurs, the device driver handler
	 * performs the specific interrupt processing for the device
	 */
	Status = XScuGic_Connect(IntcInstancePtr, IntrId,
			(Xil_ExceptionHandler) XZDma_IntrHandler,
				  (void *) InstancePtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Enable the interrupt for the device
	 */
	XScuGic_Enable(IntcInstancePtr, IntrId);

	/*
	 * Enable interrupts
	 */
	Xil_ExceptionEnableMask(XIL_EXCEPTION_IRQ);


	return XST_SUCCESS;
}


/*****************************************************************************/
/**
* This static function handles ZDMA Done interrupts.
*
* @param	CallBackRef is the callback reference passed from the interrupt
*		handler, which in our case is a pointer to the driver instance.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
static void DoneHandler(void *CallBackRef)
{
	Done = 1;

}
/*****************************************************************************/
/**
* This static function handles ZDMA error interrupts.
*
* @param	CallBackRef is the callback reference passed from the interrupt
*		handler, which in our case is a pointer to the driver instance.
* @param	Mask specifies which interrupts were occurred.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
static void ErrorHandler(void *CallBackRef, u32 Mask)
{
	Pause = 1;
}


void MemoryCopy(){

	volatile u32 ZDmaDstBuf[4096] __attribute__ ((aligned (64)));	/**< Destination buffer */

	int wordsInFifo_pre, wordsInFifo_post;

	wordsInFifo_pre = Xil_In32(FIFO_REG_RDFO_ADDR);
	for(int i= 0; i<wordsInFifo_pre; i++){
		ZDmaDstBuf[i] = Xil_In32(FIFO_STREAM_ADDR);
	}
	wordsInFifo_post = Xil_In32(FIFO_REG_RDFO_ADDR);
	printf("\r\n words in FIFO pre %d words in FIFO post %d\r\n",wordsInFifo_pre,wordsInFifo_post);
	for (int Index = 0; Index < wordsInFifo_pre; Index++) {
		printf("%08x \r\n",ZDmaDstBuf[Index]);
	}

}

