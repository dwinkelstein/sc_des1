/*
 * SC_standalone_display.c
 *
 *  Created on: Feb 16, 2021
 *      Author: DanWinkelstein
 */

#include "SC_des1.h"

enum screen_state_T {FUNCTION_SEL, VOLTAGE, CURRENT_SELECT, CURRENT_BYP, CURRENT_60A,
	                 CURRENT_60B, CURRENT_40A, CURRENT_40B, CURRENT_20A, CURRENT_20B,
					 CURRENT_MAIN, FREQUENCY, FREQ_SEL, ALARMS, SHED, RESTORE, MISC_1, MISC_2};
int screen_state = FUNCTION_SEL;

KEYPAD_VALUE_T keypad_button;


void stdA_display_voltage(SC_DES1_T *sc_des1_inst);
void stdA_display_frequency(SC_DES1_T *sc_des1_inst);
void stdA_display_current_main(SC_DES1_T *sc_des1_inst);
void stdA_display_current_60A(SC_DES1_T *sc_des1_inst);
void stdA_display_current_60B(SC_DES1_T *sc_des1_inst);
void stdA_display_current_40A(SC_DES1_T *sc_des1_inst);
void stdA_display_current_40B(SC_DES1_T *sc_des1_inst);
void stdA_display_current_20A(SC_DES1_T *sc_des1_inst);
void stdA_display_current_20B(SC_DES1_T *sc_des1_inst);
void stdA_display_current_bypass(SC_DES1_T *sc_des1_inst);
void get_voltage(SC_DES1_T *sc_des1_inst);

double volt_l1;
double volt_l2;
double volt_l3;
struct timeval tv;
time_t timeinseconds;
struct tm *tm;

// LCD display statemachine.  This display is called when a key is pressed
// The assumption is that the sc_des1_inst is an uptodate snapshot of the values in the FPGA
// input: keypad button
// output: none
void standalone_display(SC_DES1_T *sc_des1_inst){


	keypad_button = sc_des1_inst->alarm_data->keypad_value_reg;

//	printf("snapshot_v1 %x\n sensor_factor %f\n",sc_des1_inst->snapshot_data->snapshot_volt_l1, sc_des1_inst->sensor_factor->volt_factor_l1);
//	printf("snapshot_v2 %x\n sensor_factor %f\n",sc_des1_inst->snapshot_data->snapshot_volt_l2, sc_des1_inst->sensor_factor->volt_factor_l2);
//	printf("snapshot_v3 %x\n sensor_factor %f\n",sc_des1_inst->snapshot_data->snapshot_volt_l3, sc_des1_inst->sensor_factor->volt_factor_l3);

	get_voltage(sc_des1_inst);
	switch(screen_state){
	case FUNCTION_SEL:
		if(keypad_button.bits.key1Value == 1) {  //key 1 voltage
			stdA_display_voltage(sc_des1_inst);
			screen_state = VOLTAGE;
		} else if(keypad_button.bits.key2Value == 1) {  //key 2 current select
			display_current_select();
			screen_state = CURRENT_SELECT;
		} else if(keypad_button.bits.key3Value == 1) {  //key 3 frequency
			stdA_display_frequency(sc_des1_inst);
			screen_state = FREQUENCY;
		} else if(keypad_button.bits.key4Value == 1) {  //key 4 Alarms
			display_alarms(sc_des1_inst);
			screen_state = ALARMS;
		} else if(keypad_button.bits.key5Value == 1) {  //key 5 Shed
			display_shed();
			screen_state = SHED;
		} else if(keypad_button.bits.key6Value == 1) {  //key 6 Restore
			display_restore();
			screen_state = RESTORE;
		} else if(keypad_button.bits.key7Value == 1) {  //key 7 Miscellaneous
			display_misc1(sc_des1_inst->temperature, "127.000.000.001", "000.000.000.000");
			screen_state = MISC_1;
		}
        // any other key does nothing
		break;
	case VOLTAGE:
		if(keypad_button.bits.keyAValue == 1 || keypad_button.bits.keyBValue == 1) {  //key A or B key
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		// any other key does nothing
		break;
	case CURRENT_SELECT:
//		volt_l1 = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_volt_l1, sc_des1_inst->sensor_factor->volt_factor_l1);
//		volt_l2 = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_volt_l2, sc_des1_inst->sensor_factor->volt_factor_l2);
//		volt_l3 = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_volt_l3, sc_des1_inst->sensor_factor->volt_factor_l3);
		if(keypad_button.bits.key1Value == 1) {
			stdA_display_current_main(sc_des1_inst);
			screen_state = CURRENT_MAIN;
		}
		else if(keypad_button.bits.key2Value == 1) {
			stdA_display_current_60A(sc_des1_inst);
			screen_state = CURRENT_60A;
		}
		else if(keypad_button.bits.key3Value == 1) {
			stdA_display_current_60B(sc_des1_inst);
			screen_state = CURRENT_60B;
		}
		else if(keypad_button.bits.key4Value == 1) {
			stdA_display_current_40A(sc_des1_inst);
			screen_state = CURRENT_40A;
		}
		else if(keypad_button.bits.key5Value == 1) {
			stdA_display_current_40B(sc_des1_inst);
			screen_state = CURRENT_40B;
		}
		else if(keypad_button.bits.key6Value == 1) {
			stdA_display_current_20A(sc_des1_inst);
			screen_state = CURRENT_20A;
		}
		else if(keypad_button.bits.key7Value == 1) {
			stdA_display_current_20B(sc_des1_inst);
			screen_state = CURRENT_20B;
		}
		else if(keypad_button.bits.key8Value == 1) {
			stdA_display_current_bypass(sc_des1_inst);
			screen_state = CURRENT_BYP;
		}
		else if( keypad_button.bits.keyAValue == 1 || keypad_button.bits.keyBValue == 1) {  //A key or B key
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		break;
	case CURRENT_MAIN:
	case CURRENT_60A:
	case CURRENT_60B:
	case CURRENT_40A:
	case CURRENT_40B:
	case CURRENT_20A:
	case CURRENT_20B:
	case CURRENT_BYP:
		if(keypad_button.bits.keyAValue == 1 ) {  //key A key
			display_current_select();
			screen_state = CURRENT_SELECT;
		}
		else if( keypad_button.bits.keyBValue == 1) {  //key B key
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		break;
	case FREQUENCY:
		if(keypad_button.bits.keyAValue == 1 || keypad_button.bits.keyBValue == 1) {  //key A or B key
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.keyCValue == 1) {  //key C key
			int freq_setting = sc_des1_inst->alarm_data->control_reg.bits.freq60_50 == 1 ? F50HZ : F60HZ;
			display_freq_setup(freq_setting);
			screen_state = FREQ_SEL;
		}
		break;
	case FREQ_SEL:
		if(keypad_button.bits.keyAValue == 1 || keypad_button.bits.keyBValue == 1) {  //key A or B key
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key1Value == 1) {  //key 1 key  60Hz
			set_system_frequency(F60HZ, sc_des1_inst);
			display_freq_setup(F60HZ);
		}
		else if(keypad_button.bits.key2Value == 1) {  //key 2 key  50Hz
			set_system_frequency(F50HZ, sc_des1_inst);
			display_freq_setup(F50HZ);
		}
		break;
	case ALARMS:
		if(keypad_button.bits.keyAValue == 1 || keypad_button.bits.keyBValue == 1) {  //key A or B key
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		break;
	case SHED:
		if(keypad_button.bits.keyAValue == 1 || keypad_button.bits.keyBValue == 1) {  //key A or B key
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key2Value == 1) {  //key 2 shed 60A
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.f60AContactor = 1;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key3Value == 1) {  //key 3 shed 60B
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.f60BContactor = 1;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key4Value == 1) {  //key 4 shed 40A
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.f40AContactor = 1;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key5Value == 1) {  //key 5 shed 40B
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.f40BContactor = 1;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key6Value == 1) {  //key 6 shed 20A
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.f20AContactor = 1;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key7Value == 1) {  //key 7 shed 20B
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.f20BContactor = 1;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key8Value == 1) {  //key 8 shed BYP
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.bypassContactor = 1;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		break;
	case RESTORE:
		if(keypad_button.bits.keyAValue == 1 || keypad_button.bits.keyBValue == 1) {  //key A or B key
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key2Value == 1) {  //key 2 shed 60A
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.f60AContactor = 0;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key3Value == 1) {  //key 3 shed 60B
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.f60BContactor = 0;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key4Value == 1) {  //key 4 shed 40A
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.f40AContactor = 0;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key5Value == 1) {  //key 5 shed 40B
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.f40BContactor = 0;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key6Value == 1) {  //key 6 shed 20A
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.f20AContactor = 0;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key7Value == 1) {  //key 7 shed 20B
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.f20BContactor = 0;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.key8Value == 1) {  //key 8 shed BYP
			CONTACTOR_DISENGAGE_T contactor_setting;
			sc_des1_inst->alarm_data->contactor_disengage_reg.bits.bypassContactor = 0;
			contactor_setting.reg = sc_des1_inst->alarm_data->contactor_disengage_reg.reg;
			set_contactor(contactor_setting, sc_des1_inst);
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		break;
	case MISC_1:
		if(keypad_button.bits.keyAValue == 1 || keypad_button.bits.keyBValue == 1) {  //key A or B key
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		else if(keypad_button.bits.keyCValue == 1) {  //key C
			get_timestamp(sc_des1_inst);
			timeinseconds = sc_des1_inst->alarm_data->timestamp_seconds_lsb;
			tm = localtime(&timeinseconds);
			display_misc2(tm);
			screen_state = MISC_2;
		}
		break;
	case MISC_2:
		if(keypad_button.bits.keyBValue == 1 || keypad_button.bits.keyAValue == 1) {  //A key  B key
			display_function_select();
			screen_state = FUNCTION_SEL;
		}
		break;
	default:
		screen_state = FUNCTION_SEL;
		display_function_select();
		break;

	}

}

	void stdA_display_voltage(SC_DES1_T *sc_des1_inst){
		get_voltage(sc_des1_inst);
		double phasel1 = PHASE_calculation(sc_des1_inst->alarm_data->main_l1_freq_reg, sc_des1_inst->alarm_data->main_l1_phase_reg);
		double phasel2 = PHASE_calculation(sc_des1_inst->alarm_data->main_l2_freq_reg, sc_des1_inst->alarm_data->main_l2_phase_reg);
		double phasel3 = PHASE_calculation(sc_des1_inst->alarm_data->main_l3_freq_reg, sc_des1_inst->alarm_data->main_l3_phase_reg);
		display_volt(volt_l1, volt_l2, volt_l3, phasel1, phasel2, phasel3);
	}

	void stdA_display_frequency(SC_DES1_T *sc_des1_inst){
		double freql1 = FREQ_calculation(sc_des1_inst->alarm_data->main_l1_freq_reg);
		double freql2 = FREQ_calculation(sc_des1_inst->alarm_data->main_l2_freq_reg);
		double freql3 = FREQ_calculation(sc_des1_inst->alarm_data->main_l3_freq_reg);
		double phasel1 = PHASE_calculation(sc_des1_inst->alarm_data->main_l1_freq_reg, sc_des1_inst->alarm_data->main_l1_phase_reg);
		double phasel2 = PHASE_calculation(sc_des1_inst->alarm_data->main_l2_freq_reg, sc_des1_inst->alarm_data->main_l2_phase_reg);
		double phasel3 = PHASE_calculation(sc_des1_inst->alarm_data->main_l3_freq_reg, sc_des1_inst->alarm_data->main_l3_phase_reg);
		display_freq(freql1,freql2,freql3,phasel1,phasel2,phasel3);
	}

	void stdA_display_current_main(SC_DES1_T *sc_des1_inst){
		get_voltage(sc_des1_inst);
		double amp_n = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_main_n, sc_des1_inst->sensor_factor->current_main_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_main_l1, sc_des1_inst->sensor_factor->current_main_factor_l1);
		double amp_l2 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_main_l2, sc_des1_inst->sensor_factor->current_main_factor_l2);
		double amp_l3 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_main_l3, sc_des1_inst->sensor_factor->current_main_factor_l3);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst->snapshot_data->snapshot_pow_main_l1, sc_des1_inst->sensor_factor->volt_factor_l1, sc_des1_inst->sensor_factor->current_main_factor_l1);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst->snapshot_data->snapshot_pow_main_l2, sc_des1_inst->sensor_factor->volt_factor_l2, sc_des1_inst->sensor_factor->current_main_factor_l2);
		double pf_l3 =  PF_calculation(volt_l3, amp_l3, sc_des1_inst->snapshot_data->snapshot_pow_main_l3, sc_des1_inst->sensor_factor->volt_factor_l3, sc_des1_inst->sensor_factor->current_main_factor_l3);
		display_current("MAIN",amp_n, amp_l1, amp_l2, amp_l3, pf_l1, pf_l2, pf_l3);
	}

	void stdA_display_current_60A(SC_DES1_T *sc_des1_inst){
		get_voltage(sc_des1_inst);
		double amp_n = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f60a_n, sc_des1_inst->sensor_factor->current_60A_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f60a_l1, sc_des1_inst->sensor_factor->current_60A_factor_l1);
		double amp_l2 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f60a_l2, sc_des1_inst->sensor_factor->current_60A_factor_l2);
		double amp_l3 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f60a_l3, sc_des1_inst->sensor_factor->current_60A_factor_l3);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst->snapshot_data->snapshot_pow_f60a_l1, sc_des1_inst->sensor_factor->volt_factor_l1, sc_des1_inst->sensor_factor->current_60A_factor_l1);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst->snapshot_data->snapshot_pow_f60a_l2, sc_des1_inst->sensor_factor->volt_factor_l2, sc_des1_inst->sensor_factor->current_60A_factor_l2);
		double pf_l3 =  PF_calculation(volt_l3, amp_l3, sc_des1_inst->snapshot_data->snapshot_pow_f60a_l3, sc_des1_inst->sensor_factor->volt_factor_l3, sc_des1_inst->sensor_factor->current_60A_factor_l3);
		display_current("60A ",amp_n, amp_l1, amp_l2, amp_l3, pf_l1, pf_l2, pf_l3);
	}
	void stdA_display_current_60B(SC_DES1_T *sc_des1_inst){
		get_voltage(sc_des1_inst);
		double amp_n = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f60b_n, sc_des1_inst->sensor_factor->current_60B_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f60b_l1, sc_des1_inst->sensor_factor->current_60B_factor_l1);
		double amp_l2 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f60b_l2, sc_des1_inst->sensor_factor->current_60B_factor_l2);
		double amp_l3 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f60b_l3, sc_des1_inst->sensor_factor->current_60B_factor_l3);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst->snapshot_data->snapshot_pow_f60b_l1, sc_des1_inst->sensor_factor->volt_factor_l1, sc_des1_inst->sensor_factor->current_60B_factor_l1);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst->snapshot_data->snapshot_pow_f60b_l2, sc_des1_inst->sensor_factor->volt_factor_l2, sc_des1_inst->sensor_factor->current_60B_factor_l2);
		double pf_l3 =  PF_calculation(volt_l3, amp_l3, sc_des1_inst->snapshot_data->snapshot_pow_f60b_l3, sc_des1_inst->sensor_factor->volt_factor_l3, sc_des1_inst->sensor_factor->current_60B_factor_l3);
		display_current("60B ",amp_n, amp_l1, amp_l2, amp_l3, pf_l1, pf_l2, pf_l3);
	}
	void stdA_display_current_40A(SC_DES1_T *sc_des1_inst){
		get_voltage(sc_des1_inst);
		double amp_n = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f40a_n, sc_des1_inst->sensor_factor->current_40A_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f40a_l1, sc_des1_inst->sensor_factor->current_40A_factor_l1);
		double amp_l2 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f40a_l2, sc_des1_inst->sensor_factor->current_40A_factor_l2);
		double amp_l3 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f40a_l3, sc_des1_inst->sensor_factor->current_40A_factor_l3);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst->snapshot_data->snapshot_pow_f40a_l1, sc_des1_inst->sensor_factor->volt_factor_l1, sc_des1_inst->sensor_factor->current_40A_factor_l1);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst->snapshot_data->snapshot_pow_f40a_l2, sc_des1_inst->sensor_factor->volt_factor_l2, sc_des1_inst->sensor_factor->current_40A_factor_l2);
		double pf_l3 =  PF_calculation(volt_l3, amp_l3, sc_des1_inst->snapshot_data->snapshot_pow_f40a_l3, sc_des1_inst->sensor_factor->volt_factor_l3, sc_des1_inst->sensor_factor->current_40A_factor_l3);
		display_current("40A ",amp_n, amp_l1, amp_l2, amp_l3, pf_l1, pf_l2, pf_l3);
	}
	void stdA_display_current_40B(SC_DES1_T *sc_des1_inst){
		get_voltage(sc_des1_inst);
		double amp_n = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f40b_n, sc_des1_inst->sensor_factor->current_40B_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f40b_l1, sc_des1_inst->sensor_factor->current_40B_factor_l1);
		double amp_l2 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f40b_l2, sc_des1_inst->sensor_factor->current_40B_factor_l2);
		double amp_l3 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f40b_l3, sc_des1_inst->sensor_factor->current_40B_factor_l3);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst->snapshot_data->snapshot_pow_f40b_l1, sc_des1_inst->sensor_factor->volt_factor_l1, sc_des1_inst->sensor_factor->current_40B_factor_l1);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst->snapshot_data->snapshot_pow_f40b_l2, sc_des1_inst->sensor_factor->volt_factor_l2, sc_des1_inst->sensor_factor->current_40B_factor_l2);
		double pf_l3 =  PF_calculation(volt_l3, amp_l3, sc_des1_inst->snapshot_data->snapshot_pow_f40b_l3, sc_des1_inst->sensor_factor->volt_factor_l3, sc_des1_inst->sensor_factor->current_40B_factor_l3);
		display_current("40B ",amp_n, amp_l1, amp_l2, amp_l3, pf_l1, pf_l2, pf_l3);
	}
	//FIXME:  20B actually runs off L3
	void stdA_display_current_20A(SC_DES1_T *sc_des1_inst){
		get_voltage(sc_des1_inst);
		double amp_n = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f20a_n, sc_des1_inst->sensor_factor->current_20A_factor_n);
		double amp_l1 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f20a_l1, sc_des1_inst->sensor_factor->current_20A_factor_l1);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst->snapshot_data->snapshot_pow_f20a_l1, sc_des1_inst->sensor_factor->volt_factor_l1, sc_des1_inst->sensor_factor->current_20A_factor_l1);
		display_current("20A ",amp_n, amp_l1, 0.0, 0.0, pf_l1, 0.0, 0.0);
	}
	//FIXME:  20B actually runs off L1
	void stdA_display_current_20B(SC_DES1_T *sc_des1_inst){
		get_voltage(sc_des1_inst);
		double amp_n = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f20b_n, sc_des1_inst->sensor_factor->current_20B_factor_n);
		double amp_l2 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_f20b_l2, sc_des1_inst->sensor_factor->current_20B_factor_l2);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst->snapshot_data->snapshot_pow_f20b_l2, sc_des1_inst->sensor_factor->volt_factor_l2, sc_des1_inst->sensor_factor->current_20B_factor_l2);
		display_current("20B ",amp_n, 0.0, amp_l2, 0.0, 0.0, pf_l2, 0.0);
	}
	void stdA_display_current_bypass(SC_DES1_T *sc_des1_inst){
		get_voltage(sc_des1_inst);
		double amp_l1 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_byp_l1, sc_des1_inst->sensor_factor->current_BYP_factor_n);
		double amp_l2 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_byp_l2, sc_des1_inst->sensor_factor->current_BYP_factor_l1);
		double amp_l3 =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_byp_l3, sc_des1_inst->sensor_factor->current_BYP_factor_l2);
		double amp_n =  RMS_calculation(sc_des1_inst->snapshot_data->snapshot_amp_byp_n, sc_des1_inst->sensor_factor->current_BYP_factor_l3);
		double pf_l1 =  PF_calculation(volt_l1, amp_l1, sc_des1_inst->snapshot_data->snapshot_pow_byp_l1, sc_des1_inst->sensor_factor->volt_factor_l1, sc_des1_inst->sensor_factor->current_BYP_factor_l1);
		double pf_l2 =  PF_calculation(volt_l2, amp_l2, sc_des1_inst->snapshot_data->snapshot_pow_byp_l2, sc_des1_inst->sensor_factor->volt_factor_l2, sc_des1_inst->sensor_factor->current_BYP_factor_l2);
		double pf_l3 =  PF_calculation(volt_l3, amp_l3, sc_des1_inst->snapshot_data->snapshot_pow_byp_l3, sc_des1_inst->sensor_factor->volt_factor_l3, sc_des1_inst->sensor_factor->current_BYP_factor_l3);
		display_current("BYP ",amp_n, amp_l1, amp_l2, amp_l3, pf_l1, pf_l2, pf_l3);
	}


	void update_LCD_display(SC_DES1_T *sc_des1_inst){

		switch(screen_state){
		case FUNCTION_SEL:
			break;
		case VOLTAGE:
			stdA_display_voltage(sc_des1_inst);
			break;
		case CURRENT_SELECT:
			break;
		case CURRENT_BYP:
			stdA_display_current_bypass(sc_des1_inst);
			break;
		case CURRENT_60A:
			stdA_display_current_60A(sc_des1_inst);
			break;
		case CURRENT_60B:
			stdA_display_current_60B(sc_des1_inst);
			break;
		case CURRENT_40A:
			stdA_display_current_40A(sc_des1_inst);
			break;
		case CURRENT_40B:
			stdA_display_current_40B(sc_des1_inst);
			break;
		case CURRENT_20A:
			stdA_display_current_20A(sc_des1_inst);
			break;
		case CURRENT_20B:
			stdA_display_current_20B(sc_des1_inst);
			break;
		case CURRENT_MAIN:
			stdA_display_current_main(sc_des1_inst);
			break;
		case FREQUENCY:
			stdA_display_frequency(sc_des1_inst);
		case FREQ_SEL:
			break;
		case ALARMS:
			display_alarms(sc_des1_inst);
			break;
		case SHED:
			break;
		case RESTORE:
			break;
		case MISC_1:
			break;
		case MISC_2:
			get_timestamp(sc_des1_inst);
			timeinseconds = sc_des1_inst->alarm_data->timestamp_seconds_lsb;
			tm = localtime(&timeinseconds);
			display_misc2(tm);
			break;
		default:
			break;
		}
	}

void get_voltage(SC_DES1_T *sc_des1_inst){
	volt_l1 = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_volt_l1, sc_des1_inst->sensor_factor->volt_factor_l1);
	volt_l2 = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_volt_l2, sc_des1_inst->sensor_factor->volt_factor_l2);
	volt_l3 = RMS_calculation(sc_des1_inst->snapshot_data->snapshot_volt_l3, sc_des1_inst->sensor_factor->volt_factor_l3);

}

void debugPrintSnapshot(SC_DES1_T *sc_des1_inst){
	printf("SNAPSHOT time, %08x, %08x\n",sc_des1_inst->alarm_data->timestamp_seconds_lsb, sc_des1_inst->alarm_data->timestamp_nanoseconds);
	printf("V_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_volt_l1);
	printf("V_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_volt_l2);
	printf("V_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_volt_l3);
	printf("noise, %08x\n",sc_des1_inst->snapshot_data->snapshot_noise);

	printf("I_MAIN_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_main_l1);
	printf("P_MAIN_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_main_l1);
	printf("I_MAIN_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_main_l2);
	printf("P_MAIN_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_main_l2);
	printf("I_MAIN_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_main_l3);
	printf("P_MAIN_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_main_l3);
	printf("I_MAIN_N, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_main_n);

	printf("I_F60A_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f60a_l1);
	printf("P_F60A_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f60a_l1);
	printf("I_F60A_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f60a_l2);
	printf("P_F60A_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f60a_l2);
	printf("I_F60A_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f60a_l3);
	printf("P_F60A_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f60a_l3);
	printf("I_F60A_N, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f60a_n);

	printf("I_F60B_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f60b_l1);
	printf("P_F60B_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f60b_l1);
	printf("I_F60B_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f60b_l2);
	printf("P_F60B_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f60b_l2);
	printf("I_F60B_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f60b_l3);
	printf("P_F60B_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f60b_l3);
	printf("I_F60B_N, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f60b_n);

	printf("I_F40A_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f40a_l1);
	printf("P_F40A_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f40a_l1);
	printf("I_F40A_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f40a_l2);
	printf("P_F40A_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f40a_l2);
	printf("I_F40A_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f40a_l3);
	printf("P_F40A_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f40a_l3);
	printf("I_F40A_N, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f40a_n);

	printf("I_F40B_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f40b_l1);
	printf("P_F40B_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f40b_l1);
	printf("I_F40B_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f40b_l2);
	printf("P_F40B_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f40b_l2);
	printf("I_F40B_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f40b_l3);
	printf("P_F40B_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f40b_l3);
	printf("I_F40B_N, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f40b_n);

	printf("I_F20A_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f20a_l1);
	printf("P_F20A_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f20a_l1);
	printf("I_F20A_N, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f20a_n);

	printf("I_F20B_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f20b_l2);
	printf("P_F20B_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_f20b_l2);
	printf("I_F20B_N, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_f20b_n);

	printf("I_FBYP_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_byp_l1);
	printf("P_FBYP_L1, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_byp_l1);
	printf("I_FBYP_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_byp_l2);
	printf("P_FBYP_L2, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_byp_l2);
	printf("I_FBYP_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_byp_l3);
	printf("P_FBYP_L3, %08x\n",sc_des1_inst->snapshot_data->snapshot_pow_byp_l3);
	printf("I_FBYP_N, %08x\n",sc_des1_inst->snapshot_data->snapshot_amp_byp_n);


}

