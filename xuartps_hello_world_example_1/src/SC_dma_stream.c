/*
 * SC_dma_stream.c
 *
 *  Created on: Feb 19, 2021
 *      Author: DanWinkelstein
 */
#include "SC_des1.h"


/************************** Constant Definitions ******************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */

#define TESTVALUE	0x00000000 /**< For writing into source buffer */
#define BRAMADDR    FIFO_STREAM_ADDR

/**************************** Type Definitions *******************************/

/************************** Function Prototypes ******************************/


//static int SetupInterruptSystem(INTC *IntcInstancePtr,
//				XZDma *InstancePtr, u16 IntrId);
static void SCDoneHandler(void *CallBackRef);
static void SCErrorHandler(void *CallBackRef, u32 Mask);


/************************** Variable Definitions *****************************/

volatile static int Done = 0;				/**< Done flag */
volatile static int ErrorStatus = 0;			/**< Error / pause Status flag*/
//u32 AlloMem[200] __attribute__ ((aligned (64)));  // needed for linear mode only
XZDma_Config *Config;
XZDma_DataConfig Configure; /* Configuration values */
XScuGic XZDmaIntc;


/*****************************************************************************/
/**
*
* This function does a test of the data transfer in simple mode of normal mode
* on the ZDMA driver.
*
* @param	DeviceId is the XPAR_<ZDMA Instance>_DEVICE_ID value from
*		xparameters.h.
*
* @return
*		- XST_SUCCESS if successful.
*		- XST_FAILURE if failed.
*
* @note		None.
*
******************************************************************************/
int XZDma_Init(XZDma *ZdmaInstPtr, u16 DeviceId)
{

	int Status;

	/*
	 * Initialize the ZDMA driver so that it's ready to use.
	 * Look up the configuration in the config table,
	 * then initialize it.
	 */
	Config = XZDma_LookupConfig(DeviceId);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XZDma_CfgInitialize(ZdmaInstPtr, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	/*
	 * Performs the self-test to check hardware build.
	 */
	Status = XZDma_SelfTest(ZdmaInstPtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/* ZDMA has set in simple transfer of Normal mode */
//	Status = XZDma_SetMode(ZdmaInstPtr, TRUE, XZDMA_NORMAL_MODE);  // linear mode
	Status = XZDma_SetMode(ZdmaInstPtr, FALSE, XZDMA_NORMAL_MODE); // simple mode
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}


//	/* Allocated memory starting address should be 64 bit aligned */   // linear mode only
//	XZDma_CreateBDList(ZdmaInstPtr, XZDMA_LINEAR, (UINTPTR)AlloMem, 256);


	/* Interrupt call back has been set */
	XZDma_SetCallBack(ZdmaInstPtr, XZDMA_HANDLER_DONE,
				(void *)SCDoneHandler, ZdmaInstPtr);
	XZDma_SetCallBack(ZdmaInstPtr, XZDMA_HANDLER_ERROR,
				(void *)SCErrorHandler, ZdmaInstPtr);

	return XST_SUCCESS;
}

/*****************************************************************************/
/**
* This function sets up the interrupt system so interrupts can occur for the
* ZDMA. This function is application-specific. The user should modify this
* function to fit the application.
*
* @param	IntcInstancePtr is a pointer to the instance of the INTC.
* @param	InstancePtr contains a pointer to the instance of the ZDMA
*		driver which is going to be connected to the interrupt
*		controller.
* @param	IntrId is the interrupt Id and is typically
*		XPAR_<ZDMA_instance>_INTR value from xparameters.h.
*
* @return
*		- XST_SUCCESS if successful
*		- XST_FAILURE if failed
*
* @note		None.
*
****************************************************************************/
//int SetupInterruptSystem(INTC *IntcInstancePtr,
//				XZDma *InstancePtr,
//				u16 IntrId)
//{
//	int Status;
//
//
//	XScuGic_Config *IntcConfig; /* Config for interrupt controller */
//
//	/*
//	 * Initialize the interrupt controller driver
//	 */
//	IntcConfig = XScuGic_LookupConfig(ZDMA_INTC_DEVICE_ID);
//	if (NULL == IntcConfig) {
//		return XST_FAILURE;
//	}
//
//	Status = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
//					IntcConfig->CpuBaseAddress);
//	if (Status != XST_SUCCESS) {
//		return XST_FAILURE;
//	}
//
//	/*
//	 * Connect the interrupt controller interrupt handler to the
//	 * hardware interrupt handling logic in the processor.
//	 */
//	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
//			(Xil_ExceptionHandler) XScuGic_InterruptHandler,
//				IntcInstancePtr);
//
//	/*
//	 * Connect a device driver handler that will be called when an
//	 * interrupt for the device occurs, the device driver handler
//	 * performs the specific interrupt processing for the device
//	 */
//	Status = XScuGic_Connect(IntcInstancePtr, IntrId,
//			(Xil_ExceptionHandler) XZDma_IntrHandler,
//				  (void *) InstancePtr);
//	if (Status != XST_SUCCESS) {
//		return XST_FAILURE;
//	}
//
//	/*
//	 * Enable the interrupt for the device
//	 */
//	XScuGic_Enable(IntcInstancePtr, IntrId);
//
//	/*
//	 * Enable interrupts
//	 */
//	Xil_ExceptionEnableMask(XIL_EXCEPTION_IRQ);
//
//
//	return XST_SUCCESS;
//}


/*****************************************************************************/
/**
* This static function handles ZDMA Done interrupts.
*
* @param	CallBackRef is the callback reference passed from the interrupt
*		handler, which in our case is a pointer to the driver instance.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
static void SCDoneHandler(void *CallBackRef)
{
	Done = 1;

}
/*****************************************************************************/
/**
* This static function handles ZDMA error interrupts.
*
* @param	CallBackRef is the callback reference passed from the interrupt
*		handler, which in our case is a pointer to the driver instance.
* @param	Mask specifies which interrupts were occurred.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
static void SCErrorHandler(void *CallBackRef, u32 Mask)
{
	ErrorStatus = Mask;
}

//int XZDma_Read_Stream_block(u8 *ZDmaDstBuf, int size) {
///*
// * Transfer elements
// */
//	XZDma *ZdmaInstPtr = &ZDma;
//	XZDma_Transfer Data;
//
//	int requested_transfer_size = size;
//	do{
//		Done = 0;
//		ErrorStatus = 0;
//		int transferSize = (size > MAXTRANSFER) ? MAXTRANSFER : size;
//		size -= transferSize;
//		Data.DstAddr = (UINTPTR)ZDmaDstBuf;
//		Data.DstCoherent = 1;
//	//		Data.DstAddr = (UINTPTR)BRAMADDR;
//	//		Data.DstCoherent = 0;
//		Data.Pause = 0;
//	//		Data.SrcAddr = (UINTPTR)ZDmaSrcBuf;
//	//		Data.SrcCoherent = 1;
//		Data.SrcAddr = (UINTPTR)BRAMADDR;
//		Data.SrcCoherent = 0;
//		Data.Size = transferSize; /* 32-words at a time */
//
//		XZDma_Start(ZdmaInstPtr, &Data, 1); /* Initiates the data transfer */
//
//		/* Wait till DMA error or done interrupt generated */
//		while (!ErrorStatus && (Done == 0));
//		if (ErrorStatus) {
//			if (ErrorStatus & XZDMA_IXR_AXI_WR_DATA_MASK)
//				xil_printf("Pull Error occurred on write data channel\n\r");
//			if (ErrorStatus & XZDMA_IXR_AXI_RD_DATA_MASK)
//				xil_printf("Pull Error occurred on read data channel\n\r");
//			return XST_FAILURE;
//		}
//	} while(size > 0);
//	return requested_transfer_size;
//
//}
int XDMA_Transfer(XZDma *ZdmaInstPtr, u32 *bufferPtr){
	XZDma_Transfer Data[2];
//	int Index;

	/* Configuration settings */
	Configure.OverFetch = 0;
	Configure.SrcIssue = 0x1F;
	Configure.SrcBurstType = XZDMA_FIXED_BURST;
	Configure.SrcBurstLen = 0xF; //256 words
	Configure.DstBurstType = XZDMA_INCR_BURST;
	Configure.DstBurstLen = 0xF;  //256 words
	Configure.SrcCache = 0x2;
	Configure.DstCache = 0x2;
	if (Config->IsCacheCoherent) {
		Configure.SrcCache = 0xF;
		Configure.DstCache = 0xF;
	}
	Configure.SrcQos = 0;
	Configure.DstQos = 0;

	XZDma_SetChDataConfig(ZdmaInstPtr, &Configure);

	/* Enable required interrupts */
	XZDma_EnableIntr(ZdmaInstPtr, XZDMA_IXR_ALL_INTR_MASK);

	/*
	 * Transfer elements
	 */
	int wordsTransfered = 0;
//	wordsInFifo_pre = Xil_In32(FIFO_REG_RDFO_ADDR);

		Data[0].DstAddr = (UINTPTR)bufferPtr;
		Data[0].Size = TRANSFERSIZE*4; /* Size in bytes */
		Data[0].SrcAddr = FIFO_STREAM_ADDR;
		Data[0].Pause = 0;
//		Data[0].Pause = 1;     // linear mode
		if (!Config->IsCacheCoherent) {
			Data[0].DstCoherent = 1;
			Data[0].SrcCoherent = 1;
		} else {
			Data[0].DstCoherent = 0;
			Data[0].SrcCoherent = 0;
		}

//		Data[1].DstAddr = (UINTPTR)(bufferPtr + TRANSFERSIZE);   //linear mode
//		Data[1].Size = TRANSFERSIZE*4; /* Size in bytes */
//		Data[1].SrcAddr = FIFO_STREAM_ADDR;
//		Data[1].Pause = 0;
//		if (!Config->IsCacheCoherent) {
//			Data[1].DstCoherent = 1;
//			Data[1].SrcCoherent = 1;
//		} else {
//			Data[1].DstCoherent = 0;
//			Data[1].SrcCoherent = 0;
//		}


		/*
		 * Invalidating destination address and flushing
		 * source address in cache
		 */
		if (!Config->IsCacheCoherent) {
			Xil_DCacheFlushRange((INTPTR)Data[0].SrcAddr, Data[0].Size);
			Xil_DCacheInvalidateRange((INTPTR)Data[0].DstAddr, Data[0].Size);
//			Xil_DCacheFlushRange((INTPTR)Data[1].SrcAddr, Data[1].Size);   //linear mode
//			Xil_DCacheInvalidateRange((INTPTR)Data[1].DstAddr, Data[1].Size);
		}

//		XZDma_Start(ZdmaInstPtr, Data, 2); /* Initiates the data transfer */ // linear mode
		XZDma_Start(ZdmaInstPtr, &Data[0], 1); /* Initiates the data transfer */

		/* wait until pause interrupt is generated and has been resumed */
//		while (Pause == 0);  // transfer 0   // linear mode
//  		Pause = 0;
//		/* Resuming the ZDMA core */
//		XZDma_Resume(ZdmaInstPtr);
		/* Wait till DMA error or done interrupt generated */
		while (!ErrorStatus && (Done == 0));  // Transfer 1
		if (ErrorStatus) {
			if (ErrorStatus & XZDMA_IXR_AXI_WR_DATA_MASK)
				xil_printf("Error occurred on write data channel\n\r");
		}

		Done = 0;

//		wordsTransfered = (Data[0].Size + Data[1].Size)/4;  // linear
		wordsTransfered = (Data[0].Size)/4;
//		XZDma_DisableIntr(ZdmaInstPtr, XZDMA_IXR_ALL_INTR_MASK);

	return wordsTransfered;
}


/*****************************************************************************/
/**
* This function sets up the interrupt system so interrupts can occur for the
* ZDMA. This function is application-specific. The user should modify this
* function to fit the application.
*
* @param	IntcInstancePtr is a pointer to the instance of the INTC.
* @param	InstancePtr contains a pointer to the instance of the ZDMA
*		driver which is going to be connected to the interrupt
*		controller.
* @param	IntrId is the interrupt Id and is typically
*		XPAR_<ZDMA_instance>_INTR value from xparameters.h.
*
* @return
*		- XST_SUCCESS if successful
*		- XST_FAILURE if failed
*
* @note		None.
*
****************************************************************************/
int SetupXZDmaInterruptSystem_example(INTC *IntcInstancePtr, XZDma *InstancePtr, u16 IntrId)
{
	int Status;
//
//	XScuGic_Config *IntcConfig; /* Config for interrupt controller */
//
//	/*
//	 * Initialize the interrupt controller driver
//	 */
//	IntcConfig = XScuGic_LookupConfig(ZDMA_INTC_DEVICE_ID);
//	if (NULL == IntcConfig) {
//		return XST_FAILURE;
//	}
//
//	Status = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
//					IntcConfig->CpuBaseAddress);
//	if (Status != XST_SUCCESS) {
//		return XST_FAILURE;
//	}
//
//	/*
//	 * Connect the interrupt controller interrupt handler to the
//	 * hardware interrupt handling logic in the processor.
//	 */
//	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
//			(Xil_ExceptionHandler) XScuGic_InterruptHandler,
//				IntcInstancePtr);
//
	/*
	 * Connect a device driver handler that will be called when an
	 * interrupt for the device occurs, the device driver handler
	 * performs the specific interrupt processing for the device
	 */
	Status = XScuGic_Connect(IntcInstancePtr, IntrId,
			(Xil_ExceptionHandler) XZDma_IntrHandler,
				  (void *) InstancePtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Enable the interrupt for the device
	 */
	XScuGic_Enable(IntcInstancePtr, IntrId);

	/*
	 * Enable interrupts
	 */
	Xil_ExceptionEnableMask(XIL_EXCEPTION_IRQ);


	return XST_SUCCESS;
}

