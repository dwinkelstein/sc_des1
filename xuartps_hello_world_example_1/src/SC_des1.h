/*
 * SC_des1.h
 *
 *  Created on: Feb 15, 2021
 *      Author: DanWinkelstein
 */

#ifndef SC_DES1_H_
#define SC_DES1_H_

#include "xil_io.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xparameters_ps.h"
#include <stdio.h>
#include "xuartps.h"
#include <sys/time.h>
#include <time.h>
#include "xiicps.h"
#include <math.h>
#include <fastmath.h>
//#include "sleep.h"
#include "xil_mem.h"
#include "xzdma.h"
#include "xil_cache.h"
#include "xscugic.h"
//#include "platform.h"

//note: for enabling interrupts, the specific subsystem interrupt must be enabled and the global interrupt must be
// enabled.  This allows us to pick and choose the interrupt subsystem that and also to turn off all interrupts.

#define CONTROL_REG_ADDR (UINTPTR)0xA0030000
typedef union  {
	uint32_t reg;  // read/write registers
	struct   {
		uint32_t interruptEnable: 1;  //bit0: enable interrupts for the subsystems that have interrupts enabled
		uint32_t streamIntEnable: 1;  //bit 1; enable  interrupts for StreamFIFO
		uint32_t snapShotIntEnable: 1;  // bit 2 enable interrupts for snapshot data
		uint32_t alarmInterruptEnable:1; // bit 3 enable interrupts for alarms and keypad
		uint32_t unusedA: 4;
		uint32_t dataCollectEnable: 1;  //bit 8:enable data collection
		uint32_t streamCollectEnable: 1; // bit 9: enable stream collection (independent of interrupt)
		uint32_t unusedB: 6;
		uint32_t greenLEDEnable: 1;  //bit 16: LED to go on and stay on as long as access every second
		uint32_t unusedC: 7;
		uint32_t freq60_50: 1;  //bit24: 0= 60Hz system 1 = 50Hz system
		uint32_t unusedD: 7;
	} bits;
} CONTROL_REG_T;

#define VOLT_INTERRUPT_ADDR (UINTPTR)0xA0030004
typedef union {
	uint32_t reg;  //read only clear on read, interrupt event
	struct  {
		uint32_t mainL3UnderVoltage: 1; //bit0
		uint32_t mainL3OverVoltage: 1;
		uint32_t mainL2UnderVoltage: 1;
		uint32_t mainL2OverVoltage: 1;
		uint32_t mainL1UnderVoltage: 1;
		uint32_t mainL1OverVoltage: 1;
		uint32_t unusedA: 10;
		uint32_t circuitBreakerTrip: 1; //bit 16
		uint32_t unusedB: 7;
		uint32_t contactorFail: 1;  //bit 24
		uint32_t unusedC: 7;
	} bits;
} VOLT_INTERRUPT_T;

#define FREQ_INTERRUPT_ADDR (UINTPTR)0xA0030008
typedef union {
	uint32_t reg; //read only clear on read, interrupt event
	struct  {
		uint32_t mainL3UnderFreq: 1; //bit0
		uint32_t mainL3OverFreq: 1;
		uint32_t mainL2UnderFreq: 1;
		uint32_t mainL2OverFreq: 1;
		uint32_t mainL1UnderFreq: 1;
		uint32_t mainL1OverFreq: 1;
		uint32_t unusedA: 10;
		uint32_t mainL3UnderPhase: 1; //bit16
		uint32_t mainL3OverPhase: 1;
		uint32_t mainL2UnderPhase: 1;
		uint32_t mainL2OverPhase: 1;
		uint32_t mainL1UnderPhase: 1;
		uint32_t unusedB: 10;
	} bits;
} FREQ_INTERRUPT_T;

#define KEYPAD_INTERRUPT_ADDR (UINTPTR)0xA003000C
typedef union {
	uint32_t reg; //read only clear on read, interrupt event
	struct  {
		uint32_t key1Event: 1;
		uint32_t key2Event: 1;
		uint32_t key3Event: 1;
		uint32_t keyAEvent: 1;
		uint32_t key4Event: 1;
		uint32_t key5Event: 1;
		uint32_t key6Event: 1;
		uint32_t keyBEvent: 1;
		uint32_t key7Event: 1;
		uint32_t key8Event: 1;
		uint32_t key9Event: 1;
		uint32_t keyCEvent: 1;
		uint32_t keyStarEvent: 1;
		uint32_t key0Event: 1;
		uint32_t keyPoundEvent: 1;
		uint32_t keyDEvent: 1;
		uint32_t unusedA: 16;
	} bits;
} KEYPAD_INTERRUPT_T;


#define CURRENT_INTERRUPT_ADDR (UINTPTR)0xA0030010
typedef union {
	uint32_t reg; //read only clear on read, interrupt event
	struct  {
		uint32_t byp_N_Overcurrent: 1;
		uint32_t byp_L3_Overcurrent: 1;
		uint32_t byp_L2_Overcurrent: 1;
		uint32_t byp_L1_Overcurrent: 1;
		uint32_t f20B_N_Overcurrent: 1;
		uint32_t f20B_L2_Overcurrent: 1;
		uint32_t f20A_N_Overcurrent: 1;
		uint32_t f20A_L1_Overcurrent: 1;
		uint32_t f40B_N_Overcurrent: 1;
		uint32_t f40B_L3_Overcurrent: 1;
		uint32_t f40B_L2_Overcurrent: 1;
		uint32_t f40B_L1_Overcurrent: 1;
		uint32_t f40A_N_Overcurrent: 1;
		uint32_t f40A_L3_Overcurrent: 1;
		uint32_t f40A_L2_Overcurrent: 1;
		uint32_t f40A_L1_Overcurrent: 1;
		uint32_t f60B_N_Overcurrent: 1;
		uint32_t f60B_L3_Overcurrent: 1;
		uint32_t f60B_L2_Overcurrent: 1;
		uint32_t f60B_L1_Overcurrent: 1;
		uint32_t f60A_N_Overcurrent: 1;
		uint32_t f60A_L3_Overcurrent: 1;
		uint32_t f60A_L2_Overcurrent: 1;
		uint32_t f60A_L1_Overcurrent: 1;
		uint32_t main_N_Overcurrent: 1;
		uint32_t main_L3_Overcurrent: 1;
		uint32_t main_L2_Overcurrent: 1;
		uint32_t main_L1_Overcurrent: 1;
		uint32_t unusedA: 4;
	} bits;
} CURRENT_INTERRUPT_T;

#define STREAM_INTERRUPT_ADDR (UINTPTR)0xA0030014
typedef union {
	uint32_t reg; //read only clear on read, interrupt event
	struct  {
		uint32_t streamFifoInterrupt: 1;
		uint32_t snapShotUpdateInterrupt: 1;
		uint32_t unusedA: 30;
	} bits;
} STREAM_INTERRUPT_T;

#define CONTACTOR_DISENGAGE_ADDR (UINTPTR)0xA0030018
typedef union {
	uint32_t reg; //read/write register 1 = disengage contactor, i.e. shed current
	struct  {
		uint32_t bypassContactor: 1;
		uint32_t f20BContactor: 1;
		uint32_t f20AContactor: 1;
		uint32_t f40BContactor: 1;
		uint32_t f40AContactor: 1;
		uint32_t f60BContactor: 1;
		uint32_t f60AContactor: 1;
		uint32_t unusedA: 25;
	} bits;
} CONTACTOR_DISENGAGE_T;

#define CIRCUIT_BREAKER_STATUS_ADDR (UINTPTR)0xA003001C
typedef union {
	uint32_t reg; //read only register 1 = circuit breaker trip,
	struct  {
		uint32_t mainCircuitBreaker: 1;
		uint32_t f20BCircuitBreaker: 1;
		uint32_t f20ACircuitBreaker: 1;
		uint32_t f40BCircuitBreaker: 1;
		uint32_t f40ACircuitBreaker: 1;
		uint32_t f60BCircuitBreaker: 1;
		uint32_t f60ACircuitBreaker: 1;
		uint32_t unusedA: 9;
		uint32_t contactorOpen: 1;  // contactor is open (either shed or fail)
		uint32_t unusedB: 15;
	} bits;
} CIRCUIT_BREAKER_STATUS_T;

#define MAIN_L1_VOLT_ADDR (UINTPTR)0xA0030020
#define MAIN_L2_VOLT_ADDR (UINTPTR)0xA0030024
#define MAIN_L3_VOLT_ADDR (UINTPTR)0xA0030028
#define MAIN_L1_FREQ_ADDR (UINTPTR)0xA003002C
#define MAIN_L2_FREQ_ADDR (UINTPTR)0xA0030030
#define MAIN_L3_FREQ_ADDR (UINTPTR)0xA0030034
#define MAIN_L1L2_PHASE_ADDR (UINTPTR)0xA0030038
#define MAIN_L2L3_PHASE_ADDR (UINTPTR)0xA003003C
#define MAIN_L3L1_PHASE_ADDR (UINTPTR)0xA0030040
#define MAIN_L1_AMP_ADDR (UINTPTR)0xA0030044
#define MAIN_L2_AMP_ADDR (UINTPTR)0xA0030048
#define MAIN_L3_AMP_ADDR (UINTPTR)0xA003004C
#define MAIN_N_AMP_ADDR (UINTPTR)0xA0030050
#define F60A_L1_AMP_ADDR (UINTPTR)0xA0030054
#define F60A_L2_AMP_ADDR (UINTPTR)0xA0030058
#define F60A_L3_AMP_ADDR (UINTPTR)0xA003005C
#define F60A_N_AMP_ADDR (UINTPTR)0xA0030060
#define F60B_L1_AMP_ADDR (UINTPTR)0xA0030064
#define F60B_L2_AMP_ADDR (UINTPTR)0xA0030068
#define F60B_L3_AMP_ADDR (UINTPTR)0xA003006C
#define F60B_N_AMP_ADDR (UINTPTR)0xA0030070
#define F40A_L1_AMP_ADDR (UINTPTR)0xA0030074
#define F40A_L2_AMP_ADDR (UINTPTR)0xA0030078
#define F40A_L3_AMP_ADDR (UINTPTR)0xA003007C
#define F40A_N_AMP_ADDR (UINTPTR)0xA0030080
#define F40B_L1_AMP_ADDR (UINTPTR)0xA0030084
#define F40B_L2_AMP_ADDR (UINTPTR)0xA0030088
#define F40B_L3_AMP_ADDR (UINTPTR)0xA003008C
#define F40B_N_AMP_ADDR (UINTPTR)0xA0030090
#define F20A_L1_AMP_ADDR (UINTPTR)0xA0030094
#define F20A_N_AMP_ADDR (UINTPTR)0xA0030098
#define F20B_L2_AMP_ADDR (UINTPTR)0xA003009C
#define F20B_N_AMP_ADDR (UINTPTR)0xA00300A0
#define FBYP_L1_AMP_ADDR (UINTPTR)0xA00300A4
#define FBYP_L2_AMP_ADDR (UINTPTR)0xA00300A8
#define FBYP_L3_AMP_ADDR (UINTPTR)0xA00300AC
#define FBYP_N_AMP_ADDR (UINTPTR)0xA00300B0

#define MAIN_L1_VOLT_MAX_ADDR (UINTPTR)0xA00300C0
#define MAIN_L1_VOLT_MIN_ADDR (UINTPTR)0xA00300C4
#define MAIN_L2_VOLT_MAX_ADDR (UINTPTR)0xA00300C8
#define MAIN_L2_VOLT_MIN_ADDR (UINTPTR)0xA00300CC
#define MAIN_L3_VOLT_MAX_ADDR (UINTPTR)0xA00300D0
#define MAIN_L3_VOLT_MIN_ADDR (UINTPTR)0xA00300D4
#define MAIN_FREQ_MAX_ADDR (UINTPTR)0xA00300D8
#define MAIN_FREQ_MIN_ADDR (UINTPTR)0xA00300DC
#define MAIN_PHASE_MAX_ADDR (UINTPTR)0xA00300E0
#define MAIN_PHASE_MIN_ADDR (UINTPTR)0xA00300E4
#define MAIN_L1_AMP_MAX_ADDR (UINTPTR)0xA00300E8
#define MAIN_L2_AMP_MAX_ADDR (UINTPTR)0xA00300EC
#define MAIN_L3_AMP_MAX_ADDR (UINTPTR)0xA00300F0
#define MAIN_N_AMP_MAX_ADDR (UINTPTR)0xA00300F4
#define F60A_L1_AMP_MAX_ADDR (UINTPTR)0xA00300F8
#define F60A_L2_AMP_MAX_ADDR (UINTPTR)0xA00300FC
#define F60A_L3_AMP_MAX_ADDR (UINTPTR)0xA0030100
#define F60A_N_AMP_MAX_ADDR (UINTPTR)0xA0030104
#define F60B_L1_AMP_MAX_ADDR (UINTPTR)0xA0030108
#define F60B_L2_AMP_MAX_ADDR (UINTPTR)0xA003010C
#define F60B_L3_AMP_MAX_ADDR (UINTPTR)0xA0030110
#define F60B_N_AMP_MAX_ADDR (UINTPTR)0xA0030114
#define F40A_L1_AMP_MAX_ADDR (UINTPTR)0xA0030118
#define F40A_L2_AMP_MAX_ADDR (UINTPTR)0xA003011C
#define F40A_L3_AMP_MAX_ADDR (UINTPTR)0xA0030120
#define F40A_N_AMP_MAX_ADDR (UINTPTR)0xA0030124
#define F40B_L1_AMP_MAX_ADDR (UINTPTR)0xA0030128
#define F40B_L2_AMP_MAX_ADDR (UINTPTR)0xA003012C
#define F40B_L3_AMP_MAX_ADDR (UINTPTR)0xA0030130
#define F40B_N_AMP_MAX_ADDR (UINTPTR)0xA0030134
#define F20A_L1_AMP_MAX_ADDR (UINTPTR)0xA0030138
#define F20A_N_AMP_MAX_ADDR (UINTPTR)0xA003013C
#define F20B_L2_AMP_MAX_ADDR (UINTPTR)0xA0030140
#define F20B_N_AMP_MAX_ADDR (UINTPTR)0xA0030144
#define FBYP_L1_AMP_MAX_ADDR (UINTPTR)0xA0030148
#define FBYP_L2_AMP_MAX_ADDR (UINTPTR)0xA003014C
#define FBYP_L3_AMP_MAX_ADDR (UINTPTR)0xA0030150
#define FBYP_N_AMP_MAX_ADDR (UINTPTR)0xA0030154

// IPC communication occurs via 22 32-bit registers in the Alarm register space
// design intent is the SC_top software will place the display information into these registers
// generate and interrupt to another processor running on a different A53.
// the slave processor will reset the interrupt bit, read 80 characters of data and write it to the LCD display
// this frees the SC_top software from the time consuming task of writing to a serial device.
#define IPC_INTERRUPT_ADDR (UINTPTR)0xA0030190
#define IPC_DATA0_ADDR (UINTPTR)0xA0030194
#define IPC_DATA1_ADDR (UINTPTR)0xA0030198
#define IPC_DATA2_ADDR (UINTPTR)0xA003019C
#define IPC_DATA3_ADDR (UINTPTR)0xA00301A0
#define IPC_DATA4_ADDR (UINTPTR)0xA00301A4
#define IPC_DATA5_ADDR (UINTPTR)0xA00301A8
#define IPC_DATA6_ADDR (UINTPTR)0xA00301AC
#define IPC_DATA7_ADDR (UINTPTR)0xA00301B0
#define IPC_DATA8_ADDR (UINTPTR)0xA00301B4
#define IPC_DATA9_ADDR (UINTPTR)0xA00301B8
#define IPC_DATA10_ADDR (UINTPTR)0xA00301BC
#define IPC_DATA11_ADDR (UINTPTR)0xA00301C0
#define IPC_DATA12_ADDR (UINTPTR)0xA00301C4
#define IPC_DATA13_ADDR (UINTPTR)0xA00301C8
#define IPC_DATA14_ADDR (UINTPTR)0xA00301CC
#define IPC_DATA15_ADDR (UINTPTR)0xA00301D0
#define IPC_DATA16_ADDR (UINTPTR)0xA00301D4
#define IPC_DATA17_ADDR (UINTPTR)0xA00301D8
#define IPC_DATA18_ADDR (UINTPTR)0xA00301DC
#define IPC_DATA19_ADDR (UINTPTR)0xA00301E0
#define IPC_LENGTH_ADDR (UINTPTR)0xA00301E4
#define IPC_COMMAND_ADDR (UINTPTR)0xA00301E8
#define IPC_MISC0_ADDR (UINTPTR)0xA00301EC

typedef union {
	uint32_t reg; //read only clear on read, interrupt event
	struct  {
		uint32_t IPC_interrupt: 1;    //0001
		uint32_t unusedA: 31;
	} bits;
} IPC_INTERRUPT_T;

enum IPC_COMMAND {IPC_COMMAND_CLEAR, IPC_COMMAND_LCD_OFF, IPC_COMMAND_LCD_ON, IPC_COMMAND_OUTPUT};


#define KEYPAD_VALUE_ADDR (UINTPTR)0xA00301F0
typedef union {
	uint32_t reg; //read only clear on read, interrupt event
	struct  {
		uint32_t keyAValue: 1;    //0001
		uint32_t keyStarValue: 1; //0002
		uint32_t key7Value: 1;    //0004
		uint32_t key4Value: 1;    //0008
		uint32_t key1Value: 1;    //0010
		uint32_t keyPoundValue: 1;//0020
		uint32_t key9Value: 1;    //0040
		uint32_t key6Value: 1;    //0080
		uint32_t key3Value: 1;    //0100
		uint32_t key0Value: 1;    //0200
		uint32_t key8Value: 1;    //0400
		uint32_t key5Value: 1;    //0800
		uint32_t key2Value: 1;    //1000
		uint32_t keyDValue: 1;    //2000
		uint32_t keyCValue: 1;    //4000
		uint32_t keyBValue: 1;    //8000
		uint32_t unusedA: 16;
	} bits;
} KEYPAD_VALUE_T;

#define TIMESTAMP_SECOND_MSB_ADDR (UINTPTR)0xA00301F4
#define TIMESTAMP_SECOND_LSB_ADDR (UINTPTR)0xA00301F8
#define TIMESTAMP_NANOSECOND_ADDR (UINTPTR)0xA00301FC

typedef struct {
	CONTROL_REG_T			control_reg;
	VOLT_INTERRUPT_T 		volt_interrupt_reg;
	FREQ_INTERRUPT_T		freq_interrupt_reg;
	KEYPAD_INTERRUPT_T		keypad_interrupt_reg;
	CURRENT_INTERRUPT_T 	current_interrupt_reg;
	STREAM_INTERRUPT_T  	stream_interrupt_reg;
	CONTACTOR_DISENGAGE_T 	contactor_disengage_reg;
	CIRCUIT_BREAKER_STATUS_T circuit_breaker_status_reg;
	uint32_t				main_l1_volt_reg;
	uint32_t				main_l2_volt_reg;
	uint32_t				main_l3_volt_reg;
	uint32_t				main_l1_freq_reg;
	uint32_t				main_l2_freq_reg;
	uint32_t				main_l3_freq_reg;
	uint32_t				main_l1_phase_reg;
	uint32_t				main_l2_phase_reg;
	uint32_t				main_l3_phase_reg;
	uint32_t				main_l1_amp_reg;
	uint32_t				main_l2_amp_reg;
	uint32_t				main_l3_amp_reg;
	uint32_t				main_n_amp_reg;
	uint32_t				f60a_l1_amp_reg;
	uint32_t				f60a_l2_amp_reg;
	uint32_t				f60a_l3_amp_reg;
	uint32_t				f60a_n_amp_reg;
	uint32_t				f60b_l1_amp_reg;
	uint32_t				f60b_l2_amp_reg;
	uint32_t				f60b_l3_amp_reg;
	uint32_t				f60b_n_amp_reg;
	uint32_t				f40a_l1_amp_reg;
	uint32_t				f40a_l2_amp_reg;
	uint32_t				f40a_l3_amp_reg;
	uint32_t				f40a_n_amp_reg;
	uint32_t				f40b_l1_amp_reg;
	uint32_t				f40b_l2_amp_reg;
	uint32_t				f40b_l3_amp_reg;
	uint32_t				f40b_n_amp_reg;
	uint32_t				f20a_l1_amp_reg;
	uint32_t				f20a_n_amp_reg;
	uint32_t				f20b_l2_amp_reg;
	uint32_t				f20b_n_amp_reg;
	uint32_t				fbyp_l1_amp_reg;
	uint32_t				fbyp_l2_amp_reg;
	uint32_t				fbyp_l3_amp_reg;
	uint32_t				fbyp_n_amp_reg;
	uint32_t unusedA[3];
	uint32_t				main_l1_volt_max_reg;
	uint32_t				main_l1_volt_min_reg;
	uint32_t				main_l2_volt_max_reg;
	uint32_t				main_l2_volt_min_reg;
	uint32_t				main_l3_volt_max_reg;
	uint32_t				main_l3_volt_min_reg;
	uint32_t				main_freq_max_reg;
	uint32_t				main_freq_min_reg;
	uint32_t				main_phase_max_reg;
	uint32_t				main_phase_min_reg;
	uint32_t				main_l1_amp_max_reg;
	uint32_t				main_l2_amp_max_reg;
	uint32_t				main_l3_amp_max_reg;
	uint32_t				main_n_amp_max_reg;
	uint32_t				f60a_l1_amp_max_reg;
	uint32_t				f60a_l2_amp_max_reg;
	uint32_t				f60a_l3_amp_max_reg;
	uint32_t				f60a_n_amp_max_reg;
	uint32_t				f60b_l1_amp_max_reg;
	uint32_t				f60b_l2_amp_max_reg;
	uint32_t				f60b_l3_amp_max_reg;
	uint32_t				f60b_n_amp_max_reg;
	uint32_t				f40a_l1_amp_max_reg;
	uint32_t				f40a_l2_amp_max_reg;
	uint32_t				f40a_l3_amp_max_reg;
	uint32_t				f40a_n_amp_max_reg;
	uint32_t				f40b_l1_amp_max_reg;
	uint32_t				f40b_l2_amp_max_reg;
	uint32_t				f40b_l3_amp_max_reg;
	uint32_t				f40b_n_amp_max_reg;
	uint32_t				f20a_l1_amp_max_reg;
	uint32_t				f20a_n_amp_max_reg;
	uint32_t				f20b_l2_amp_max_reg;
	uint32_t				f20b_n_amp_max_reg;
	uint32_t				fbyp_l1_amp_max_reg;
	uint32_t				fbyp_l2_amp_max_reg;
	uint32_t				fbyp_l3_amp_max_reg;
	uint32_t				fbyp_n_amp_max_reg;
	uint32_t unusedB[26];
	KEYPAD_VALUE_T			keypad_value_reg;
	uint32_t				timestamp_seconds_msb;
	uint32_t				timestamp_seconds_lsb;
	uint32_t				timestamp_nanoseconds;

} ALARM_REGISTER_T;

typedef struct {
	uint32_t				main_l1_volt_reg;
	uint32_t				main_l2_volt_reg;
	uint32_t				main_l3_volt_reg;
	uint32_t				main_l1_freq_reg;
	uint32_t				main_l2_freq_reg;
	uint32_t				main_l3_freq_reg;
	uint32_t				main_l1_phase_reg;
	uint32_t				main_l2_phase_reg;
	uint32_t				main_l3_phase_reg;
	uint32_t				main_l1_amp_reg;
	uint32_t				main_l2_amp_reg;
	uint32_t				main_l3_amp_reg;
	uint32_t				main_n_amp_reg;
	uint32_t				f60a_l1_amp_reg;
	uint32_t				f60a_l2_amp_reg;
	uint32_t				f60a_l3_amp_reg;
	uint32_t				f60a_n_amp_reg;
	uint32_t				f60b_l1_amp_reg;
	uint32_t				f60b_l2_amp_reg;
	uint32_t				f60b_l3_amp_reg;
	uint32_t				f60b_n_amp_reg;
	uint32_t				f40a_l1_amp_reg;
	uint32_t				f40a_l2_amp_reg;
	uint32_t				f40a_l3_amp_reg;
	uint32_t				f40a_n_amp_reg;
	uint32_t				f40b_l1_amp_reg;
	uint32_t				f40b_l2_amp_reg;
	uint32_t				f40b_l3_amp_reg;
	uint32_t				f40b_n_amp_reg;
	uint32_t				f20a_l1_amp_reg;
	uint32_t				f20a_n_amp_reg;
	uint32_t				f20b_l2_amp_reg;
	uint32_t				f20b_n_amp_reg;
	uint32_t				fbyp_l1_amp_reg;
	uint32_t				fbyp_l2_amp_reg;
	uint32_t				fbyp_l3_amp_reg;
	uint32_t				fbyp_n_amp_reg;
	uint32_t				timestamp_seconds_msb;
	uint32_t				timestamp_seconds_lsb;
	uint32_t				timestamp_nanoseconds;
} ONE_CYCLE_SNAPSHOT_T;

typedef struct {
	uint32_t				main_l1_volt_max_reg;
	uint32_t				main_l1_volt_min_reg;
	uint32_t				main_l2_volt_max_reg;
	uint32_t				main_l2_volt_min_reg;
	uint32_t				main_l3_volt_max_reg;
	uint32_t				main_l3_volt_min_reg;
	uint32_t				main_freq_max_reg;
	uint32_t				main_freq_min_reg;
	uint32_t				main_phase_max_reg;
	uint32_t				main_phase_min_reg;
	uint32_t				main_l1_amp_max_reg;
	uint32_t				main_l2_amp_max_reg;
	uint32_t				main_l3_amp_max_reg;
	uint32_t				main_n_amp_max_reg;
	uint32_t				f60a_l1_amp_max_reg;
	uint32_t				f60a_l2_amp_max_reg;
	uint32_t				f60a_l3_amp_max_reg;
	uint32_t				f60a_n_amp_max_reg;
	uint32_t				f60b_l1_amp_max_reg;
	uint32_t				f60b_l2_amp_max_reg;
	uint32_t				f60b_l3_amp_max_reg;
	uint32_t				f60b_n_amp_max_reg;
	uint32_t				f40a_l1_amp_max_reg;
	uint32_t				f40a_l2_amp_max_reg;
	uint32_t				f40a_l3_amp_max_reg;
	uint32_t				f40a_n_amp_max_reg;
	uint32_t				f40b_l1_amp_max_reg;
	uint32_t				f40b_l2_amp_max_reg;
	uint32_t				f40b_l3_amp_max_reg;
	uint32_t				f40b_n_amp_max_reg;
	uint32_t				f20a_l1_amp_max_reg;
	uint32_t				f20a_n_amp_max_reg;
	uint32_t				f20b_l2_amp_max_reg;
	uint32_t				f20b_n_amp_max_reg;
	uint32_t				fbyp_l1_amp_max_reg;
	uint32_t				fbyp_l2_amp_max_reg;
	uint32_t				fbyp_l3_amp_max_reg;
	uint32_t				fbyp_n_amp_max_reg;
} ALARM_LIMIT_T;

#define SNAPSHOT_VOLT_L1_ADDR 	(UINTPTR)0xA0010008
#define SNAPSHOT_VOLT_L2_ADDR 	(UINTPTR)0xA0010010
#define SNAPSHOT_VOLT_L3_ADDR 	(UINTPTR)0xA0010018
#define SNAPSHOT_NOISE_ADDR 	(UINTPTR)0xA0010020
#define SNAPSHOT_AMP_MAIN_L1 	(UINTPTR)0xA0010028
#define SNAPSHOT_POW_MAIN_L1 	(UINTPTR)0xA001002C
#define SNAPSHOT_AMP_MAIN_L2 	(UINTPTR)0xA0010030
#define SNAPSHOT_POW_MAIN_L2 	(UINTPTR)0xA0010034
#define SNAPSHOT_AMP_MAIN_L3 	(UINTPTR)0xA0010038
#define SNAPSHOT_POW_MAIN_L3 	(UINTPTR)0xA001003C
#define SNAPSHOT_AMP_MAIN_N     (UINTPTR)0xA0010040
#define SNAPSHOT_AMP_F60A_L1 	(UINTPTR)0xA0010048
#define SNAPSHOT_POW_F60A_L1 	(UINTPTR)0xA001004C
#define SNAPSHOT_AMP_F60A_L2 	(UINTPTR)0xA0010050
#define SNAPSHOT_POW_F60A_L2 	(UINTPTR)0xA0010054
#define SNAPSHOT_AMP_F60A_L3 	(UINTPTR)0xA0010058
#define SNAPSHOT_POW_F60A_L3 	(UINTPTR)0xA001005C
#define SNAPSHOT_AMP_F60A_N     (UINTPTR)0xA0010060
#define SNAPSHOT_AMP_F60B_L1 	(UINTPTR)0xA0010068
#define SNAPSHOT_POW_F60B_L1 	(UINTPTR)0xA001006C
#define SNAPSHOT_AMP_F60B_L2 	(UINTPTR)0xA0010070
#define SNAPSHOT_POW_F60B_L2 	(UINTPTR)0xA0010074
#define SNAPSHOT_AMP_F60B_L3 	(UINTPTR)0xA0010078
#define SNAPSHOT_POW_F60B_L3 	(UINTPTR)0xA001007C
#define SNAPSHOT_AMP_F60B_N     (UINTPTR)0xA0010080
#define SNAPSHOT_AMP_F40A_L1 	(UINTPTR)0xA0010088
#define SNAPSHOT_POW_F40A_L1 	(UINTPTR)0xA001008C
#define SNAPSHOT_AMP_F40A_L2 	(UINTPTR)0xA0010090
#define SNAPSHOT_POW_F40A_L2 	(UINTPTR)0xA0010094
#define SNAPSHOT_AMP_F40A_L3 	(UINTPTR)0xA0010098
#define SNAPSHOT_POW_F40A_L3 	(UINTPTR)0xA001009C
#define SNAPSHOT_AMP_F40A_N     (UINTPTR)0xA00100A0
#define SNAPSHOT_AMP_F40B_L1 	(UINTPTR)0xA00100A8
#define SNAPSHOT_POW_F40B_L1 	(UINTPTR)0xA00100AC
#define SNAPSHOT_AMP_F40B_L2 	(UINTPTR)0xA00100B0
#define SNAPSHOT_POW_F40B_L2 	(UINTPTR)0xA00100B4
#define SNAPSHOT_AMP_F40B_L3 	(UINTPTR)0xA00100B8
#define SNAPSHOT_POW_F40B_L3 	(UINTPTR)0xA00100BC
#define SNAPSHOT_AMP_F40B_N     (UINTPTR)0xA00100C0
#define SNAPSHOT_AMP_F20A_L1 	(UINTPTR)0xA00100C8
#define SNAPSHOT_POW_F20A_L1 	(UINTPTR)0xA00100CC
#define SNAPSHOT_AMP_F20A_N		(UINTPTR)0xA00100D0
#define SNAPSHOT_AMP_F20B_L2 	(UINTPTR)0xA00100D8
#define SNAPSHOT_POW_F20B_L2 	(UINTPTR)0xA00100DC
#define SNAPSHOT_AMP_F20B_N 	(UINTPTR)0xA00100E0
#define SNAPSHOT_AMP_FBYP_L1 	(UINTPTR)0xA00100E8
#define SNAPSHOT_POW_FBYP_L1 	(UINTPTR)0xA00100EC
#define SNAPSHOT_AMP_FBYP_L2 	(UINTPTR)0xA00100F0
#define SNAPSHOT_POW_FBYP_L2 	(UINTPTR)0xA00100F4
#define SNAPSHOT_AMP_FBYP_L3 	(UINTPTR)0xA00100F8
#define SNAPSHOT_POW_FBYP_L3 	(UINTPTR)0xA00100FC
#define SNAPSHOT_AMP_FBYP_N     (UINTPTR)0xA0010100

typedef struct {
	uint32_t unusedA[2];
	uint32_t snapshot_volt_l1; //0008
	uint32_t unusedB;
	uint32_t snapshot_volt_l2; //0010
	uint32_t unusedC;
	uint32_t snapshot_volt_l3; //0018
	uint32_t unusedD;
	uint32_t snapshot_noise;   //0020
	uint32_t unusedE;
	uint32_t snapshot_amp_main_l1;  //0028
	uint32_t snapshot_pow_main_l1;  //002C
	uint32_t snapshot_amp_main_l2;  //0030
	uint32_t snapshot_pow_main_l2;  //0034
	uint32_t snapshot_amp_main_l3;  //0038
	uint32_t snapshot_pow_main_l3;  //003C
	uint32_t snapshot_amp_main_n;   //0040
	uint32_t unusedF;
	uint32_t snapshot_amp_f60a_l1;  //0048
	uint32_t snapshot_pow_f60a_l1;  //004C
	uint32_t snapshot_amp_f60a_l2;  //0050
	uint32_t snapshot_pow_f60a_l2;  //0054
	uint32_t snapshot_amp_f60a_l3;  //0058
	uint32_t snapshot_pow_f60a_l3;  //005C
	uint32_t snapshot_amp_f60a_n;   //0060
	uint32_t unusedG;
	uint32_t snapshot_amp_f60b_l1;  //0068
	uint32_t snapshot_pow_f60b_l1;  //006C
	uint32_t snapshot_amp_f60b_l2;  //0070
	uint32_t snapshot_pow_f60b_l2;  //0074
	uint32_t snapshot_amp_f60b_l3;  //0078
	uint32_t snapshot_pow_f60b_l3;  //007C
	uint32_t snapshot_amp_f60b_n;   //0080
	uint32_t unusedH;
	uint32_t snapshot_amp_f40a_l1;  //0088
	uint32_t snapshot_pow_f40a_l1;  //008C
	uint32_t snapshot_amp_f40a_l2;  //0090
	uint32_t snapshot_pow_f40a_l2;  //0094
	uint32_t snapshot_amp_f40a_l3;  //0098
	uint32_t snapshot_pow_f40a_l3;  //009C
	uint32_t snapshot_amp_f40a_n;   //00A0
	uint32_t unusedI;
	uint32_t snapshot_amp_f40b_l1;  //00A8
	uint32_t snapshot_pow_f40b_l1;  //00AC
	uint32_t snapshot_amp_f40b_l2;  //00B0
	uint32_t snapshot_pow_f40b_l2;  //00B4
	uint32_t snapshot_amp_f40b_l3;  //00B8
	uint32_t snapshot_pow_f40b_l3;  //00BC
	uint32_t snapshot_amp_f40b_n;   //00C0
	uint32_t unusedJ;
	uint32_t snapshot_amp_f20a_l1;  //00C8
	uint32_t snapshot_pow_f20a_l1;  //00CC
	uint32_t snapshot_amp_f20a_n;   //00D0
	uint32_t unusedK;
	uint32_t snapshot_amp_f20b_l2;  //00D8
	uint32_t snapshot_pow_f20b_l2;  //00DC
	uint32_t snapshot_amp_f20b_n;   //00E0
	uint32_t unusedL;
	uint32_t snapshot_amp_byp_l1;  //00E8
	uint32_t snapshot_pow_byp_l1;  //00EC
	uint32_t snapshot_amp_byp_l2;  //00F0
	uint32_t snapshot_pow_byp_l2;  //00F4
	uint32_t snapshot_amp_byp_l3;  //00F8
	uint32_t snapshot_pow_byp_l3;  //00FC
	uint32_t snapshot_amp_byp_n;   //0100
	uint32_t unusedM[15];          // align to 64-byte boundaries
} SNAPSHOT_MEMORY_T;

typedef struct {
	double volt_factor_l1;
	double volt_factor_l2;
	double volt_factor_l3;
	double current_main_factor_l1;
	double current_main_factor_l2;
	double current_main_factor_l3;
	double current_main_factor_n;
	double current_60A_factor_l1;
	double current_60A_factor_l2;
	double current_60A_factor_l3;
	double current_60A_factor_n;
	double current_60B_factor_l1;
	double current_60B_factor_l2;
	double current_60B_factor_l3;
	double current_60B_factor_n;
	double current_40A_factor_l1;
	double current_40A_factor_l2;
	double current_40A_factor_l3;
	double current_40A_factor_n;
	double current_40B_factor_l1;
	double current_40B_factor_l2;
	double current_40B_factor_l3;
	double current_40B_factor_n;
	double current_20A_factor_l1;
	double current_20A_factor_n;
	double current_20B_factor_l2;
	double current_20B_factor_n;
	double current_BYP_factor_l1;
	double current_BYP_factor_l2;
	double current_BYP_factor_l3;
	double current_BYP_factor_n;
} SENSOR_FACTOR_T;

#define PCA9539_INPUT 0xFF  // all pins for a PCA9539 port are high impedance inputs
#define PCA9539_OUTPUT 0x00  // all pins for a PCA9539 port are high impedance inputs


typedef union {
	uint8_t reg;
	struct {
		uint8_t cb_trip_main : 1;
		uint8_t cb_trip_60A : 1;
		uint8_t cb_trip_60B : 1;
		uint8_t cb_trip_40A : 1;
		uint8_t cb_trip_40B : 1;
		uint8_t cb_trip_20A : 1;
		uint8_t cb_trip_20B : 1;
		uint8_t led_off : 1;
	} bits;
} PCA9539_PORT0_T;

typedef union {
	uint8_t reg;
	struct {
		uint8_t contactor_fail : 1;
		uint8_t contactor_open_60A : 1;
		uint8_t contactor_open_60B : 1;
		uint8_t contactor_open_40A : 1;
		uint8_t contactor_open_40B : 1;
		uint8_t contactor_open_20A : 1;
		uint8_t contactor_open_20B : 1;
		uint8_t contactor_open_BYP : 1;
	} bits;
} PCA9539_PORT1_T;


typedef struct {
	PCA9539_PORT0_T port0;
	PCA9539_PORT1_T port1;
} PCA9539_T;

#define SAMPLES_PER_CYCLE 4096
#define FCLOCKS_PER_SECOND 122880000.0

#define SNAPSHOT_MEMORY_ADDR (UINTPTR)0xA0010000
#define SNAPSHOT_MEMORY_SIZE sizeof(SNAPSHOT_MEMORY_T)
#define ALARM_MEMORY_ADDR (UINTPTR)0xA0030000
#define ALARM_MEMORY_SIZE sizeof(ALARM_REGISTER_T)
#define ALARM_DATA_SIZE 156 // contactor disengage reg to fbyp_n_amp_reg [FIXME]
#define FIFO_STREAM_ADDR (UINTPTR)0xA0022000
#define FIFO_STREAM_SIZE 0x00010000
#define FIFO_REG_ADDR (UINTPTR)0xA0000000
#define FIFO_REG_SIZE 0x80

//the following are registers for the stream FIFO control and status
#define FIFO_REG_ISR_ADDR (FIFO_REG_ADDR + 0x0)
#define FIFO_REG_IER_ADDR (FIFO_REG_ADDR + 0x4)
#define FIFO_REG_TDFR_ADDR (FIFO_REG_ADDR + 0x8)
#define FIFO_REG_TDFV_ADDR (FIFO_REG_ADDR + 0xC)
#define FIFO_REG_TDFD_ADDR (FIFO_REG_ADDR + 0x10)
#define FIFO_REG_TLR_ADDR (FIFO_REG_ADDR + 0x14)
#define FIFO_REG_RDFR_ADDR (FIFO_REG_ADDR + 0x18)
#define FIFO_REG_RDFO_ADDR (FIFO_REG_ADDR + 0x1C)
#define FIFO_REG_RDFD_ADDR (FIFO_REG_ADDR + 0x20)
#define FIFO_REG_RLR_ADDR (FIFO_REG_ADDR + 0x24)
#define FIFO_REG_SRR_ADDR (FIFO_REG_ADDR + 0x28)
#define FIFO_REG_TDR_ADDR (FIFO_REG_ADDR + 0x2C)
#define FIFO_REG_RDR_ADDR (FIFO_REG_ADDR + 0x30)

typedef struct {
	SNAPSHOT_MEMORY_T *snapshot_data;
	ALARM_REGISTER_T *alarm_data;
	SENSOR_FACTOR_T *sensor_factor;
	double temperature;
	PCA9539_T *pca9539;
} SC_DES1_T;


// Function prototypes for Screen I/O
void display_function_select();
void display_volt(double volt_l1, double volt_l2, double volt_l3, double phasel1, double phasel2, double phasel3);
void display_current_select();
void display_current(char* feed, double amp_n, double amp_l1, double pf_l1, double amp_l2, double pf_l2, double amp_l3, double pf_l3);
void display_freq(double freq_l1, double phase_l1, double freq_l2, double phase_l2, double freq_l3, double phase_l3);
void display_freq_setup(int freq_setting);
void display_alarms(SC_DES1_T *sc_des1_inst);
void display_shed();
void display_restore();
void display_misc1(double temp, char *ip_address, char *controller);
void display_misc2(struct tm *tm);
void update_LCD_display(SC_DES1_T *sc_des1_inst);
void standalone_display(SC_DES1_T *sc_des1_inst);
void display_off();
void display_on();
void debugPrintSnapshot(SC_DES1_T *sc_des1_inst);

// Function prototypes for voltage/current/pf/freq/phase calculations
double RMS_calculation(uint32_t reg_value, double factor);
double PF_calculation(double voltRMS, double currentRMS, uint32_t reg_value, double volt_factor, double current_factor);
double PHASE_calculation(uint32_t freq_reg_value, uint32_t phase_reg_value);
double FREQ_calculation(uint32_t reg_value);
double NOISE_calculation(uint32_t reg_value);
double VOLT208_calculation(double volt1, double volt2, double phase);

enum FREQ_SETTING {F60HZ=0, F50HZ=1};
void set_system_frequency(int freq_setting, SC_DES1_T *sc_des1_inst);
//int  get_system_frequency(SC_DES1_T *sc_des1_inst);

enum CONTACTOR {CONT_BYP=1, CONT_F20B=2, CONT_F20A=4, CONT_F40B=8, CONT_F40A=16, CONT_60B=32, CONT_60A=64 };
void set_contactor(CONTACTOR_DISENGAGE_T contactor_setting, SC_DES1_T *sc_des1_inst);

// Initialization prototypes
#define LCD_UART_DEVICE_ID XPAR_PSU_UART_1_DEVICE_ID
int UartPsLCDinit(u16 DeviceID);
#define IIC_DEVICE_ID		XPAR_XIICPS_0_DEVICE_ID
int SC_I2C_Init(int DeviceID);
#define ZDMA_DEVICE_ID		XPAR_XZDMA_0_DEVICE_ID /* ZDMA device Id */
#define INTC		XScuGic
#define ZDMA_INTC_DEVICE_ID	XPAR_SCUGIC_SINGLE_DEVICE_ID /**< SCUGIC Device ID */
#define ZDMA_INTR_DEVICE_ID	XPAR_XADMAPS_0_INTR /**< ZDMA Interrupt Id */

int XZDma_Init(XZDma *InstancePtr, u16 DeviceId);
int externalDMAInit();
int XDMA_Transfer(XZDma *ZdmaInstPtr, u32 *bufferPtr);
int XZDma_SimpleExample(INTC *IntcInstPtr, XZDma *ZdmaInstPtr,	u16 DeviceId, u16 IntrId);
int XDMA_Tranfer_example(XZDma *ZdmaInstPtr, u32 *BufferPtr, int startIndex);
int SetupXZDmaInterruptSystem_example(INTC *IntcInstancePtr,	XZDma *InstancePtr,	u16 IntrId);

#define TRANSFERSIZE		256     /**< Size of the data to be transferred per DMA operation (32 words, 16 quads) */


//I2C operational prototypes
int get_temperature(SC_DES1_T *sc_des1_inst);  // I2C function read temperature
int get_pca9539_status(SC_DES1_T *sc_des1_inst);  // I2C function read CB trip and Contactor Open
int set_pca9539_LED(int off_on, SC_DES1_T *sc_des1_inst); // I2C function to turn LED's OFF = 0 or ON = 1
int config_pca9539(u8 config0, u8 config1);

// PIO based operations to the hardware
void get_snapshot_data(SC_DES1_T *sc_des1_inst);  // get all the data in the snapshot memory and put it into the datastructure
void get_alarm_data(SC_DES1_T *sc_des1_inst); // get all the data in the alarm memory and put it into the datastructure
void set_alarm_limits(SC_DES1_T *sc_des1_inst, ALARM_LIMIT_T *alarm_limits_value); //alarm limits populated in
void set_timestamp(SC_DES1_T *sc_des1_inst);  //set by time of day, no input required.
void get_interrupt_status(SC_DES1_T *sc_des1_inst);
void set_control_reg(SC_DES1_T *sc_des1_inst, CONTROL_REG_T *control_reg_value);
void get_control_reg(SC_DES1_T *sc_des1_inst);
void get_contactor_reg(SC_DES1_T *sc_des1_inst);
void get_circuitbreaker_reg(SC_DES1_T *sc_des1_inst);
void get_timestamp(SC_DES1_T *sc_des1_inst);
void set_timestamp_direct(SC_DES1_T *sc_des1_inst, u64 seconds, u32 nanoseconds);
void get_keypad_values(SC_DES1_T *sc_des1_inst);

// FPGA Interrupt service routings
void fabric_interrupt_init();
void fabric_interrupt_handler(void *CallBackRef, u32 Event);

// for IPC communication
void Standalone_LCD();


#define SNAPSHOT
//#define DMATEST
//#define IPCTEST
//#define IPC_DISPLAY

#endif /* SC_DES1_H_ */
