/*
 * Standalone_LCD.c
 *
 *  Created on: Apr 7, 2021
 *      Author: DanWinkelstein
 */

// Include files

#include "SC_des1.h"

// Definitions
#define IPC_INTERRUPT_ID XPAR_FABRIC_IV_ALARM_MONITOR_V1_0_0_IPC_INTERRUPT_INTR
#define IPC_DEVICE_ID XPAR_SCUGIC_0_DEVICE_ID
#define CLEAR_LCD_WAIT 1500


//Function Prototypes
void IPC_interrupt_init();
void IPC_interrupt_handler(void *CallBackRef, u32 Event);
void IPC_display_on();
void IPC_display_off();
void IPC_clear_LCD();
int IPC_LCD_output(char write_buffer[], int buffer_size);
int IPC_UartPsLCDinit(u16 DeviceID);

//Global variables
int IPC_interrupt_event = 0;
char write_buffer[80];
int IPC_command;
int buffer_length;
XUartPs Uart_PS;		/* Instance of the UART Device */
XScuGic xInterruptController;
XScuGic_Config *pxInterruptControllerConfig;



void Standalone_LCD(){
	IPC_UartPsLCDinit(LCD_UART_DEVICE_ID);
	IPC_interrupt_init();  // initialize interrupts
	while(1){
		while(IPC_interrupt_event == 0);  // wait for interrupt
		switch(IPC_command) {
		case IPC_COMMAND_CLEAR:
			IPC_clear_LCD();
			break;
		case IPC_COMMAND_LCD_OFF:
			IPC_display_off();
			break;
		case IPC_COMMAND_LCD_ON:
			IPC_display_on();
			break;
		case IPC_COMMAND_OUTPUT:
			IPC_LCD_output(write_buffer,buffer_length);
			break;
		default:
			break;
		}
		IPC_interrupt_event =0;
	}
}


void IPC_interrupt_init(){

	Xil_ExceptionInit();
/* Initialize the interrupt controller driver. */
	pxInterruptControllerConfig = XScuGic_LookupConfig( IPC_DEVICE_ID );

	XScuGic_CfgInitialize( &xInterruptController,
			               pxInterruptControllerConfig,
						   pxInterruptControllerConfig->CpuBaseAddress);
/* Connect the interrupt controller interrupt handler to the hardware */
/* interrupt handling logic in the ARM processor. */
	Xil_ExceptionRegisterHandler( XIL_EXCEPTION_ID_IRQ_INT,
                               ( Xil_ExceptionHandler ) XScuGic_InterruptHandler,
                               &xInterruptController);

/*now connect your handler*/
	XScuGic_Connect(&xInterruptController, IPC_INTERRUPT_ID,
			        (Xil_InterruptHandler) fabric_interrupt_handler, NULL);

	XScuGic_Enable(&xInterruptController, IPC_INTERRUPT_ID);
//OPTIONAL
	//XScuGic_InterruptMaptoCpu(GICInstance, XPAR_CPU_ID, irqNo);
	//XScuGic_SetPriorityTriggerType(GICInstance, irqNo, (configMAX_API_CALL_INTERRUPT_PRIORITY+1)<<3, 3);

/* Enable interrupts in the ARM. */
	Xil_ExceptionEnable();

}

void IPC_interrupt_handler(void *CallBackRef, u32 Event){

	IPC_command = Xil_In32(IPC_COMMAND_ADDR);
	if(IPC_command == IPC_COMMAND_OUTPUT){
		buffer_length = Xil_In32(IPC_LENGTH_ADDR);
		Xil_MemCpy(write_buffer,IPC_DATA0_ADDR, buffer_length);
	}
    Xil_Out32(IPC_INTERRUPT_ADDR, 0);  // clear interrupt
	IPC_interrupt_event = 1;

}

void IPC_clear_LCD(){
	char *clearScreen = "\xFE\x51";
	XUartPs_Send(&Uart_PS, (u8 *) clearScreen, (u32) 2);
	usleep(CLEAR_LCD_WAIT);  // sleep 1.5ms after clearing screen
}

#define MAX(a,b) (a>b?a:b)
#define MIN(a,b) (a<b?a:b)

int IPC_LCD_output(char write_buffer[], int buffer_size){
	/* Block sending the buffer. */
	u8 *setCursorLine0 = "\xFE\x45\x00";
	u8 *setCursorLine1 = "\xFE\x45\x40";
	u8 *setCursorLine2 = "\xFE\x45\x14";
	u8 *setCursorLine3 = "\xFE\x45\x54";
	int buffer_offset = 0;
	u32 SentCount;

	if(buffer_size > 80) buffer_size = 80;
	while(buffer_size > 0){
		if(buffer_offset < 20) {
			SentCount = XUartPs_Send(&Uart_PS, (u8 *) setCursorLine0, (u32) 3);
		} else if(buffer_offset < 40){
			SentCount = XUartPs_Send(&Uart_PS, (u8 *) setCursorLine1, (u32) 3);
		} else if(buffer_offset < 60) {
			SentCount = XUartPs_Send(&Uart_PS, (u8 *) setCursorLine2, (u32) 3);
		} else {
			SentCount = XUartPs_Send(&Uart_PS, (u8 *) setCursorLine3, (u32) 3);
		}
		usleep(10000);
		for(int i = 0; i < MIN(20,buffer_size); i++) {
			/*u32 SentCount = */ XUartPs_Send(&Uart_PS, (u8 *) &write_buffer[buffer_offset + i], (u32) 1);
			usleep(1000);
		}
		buffer_size -= MIN(20,buffer_size);
		buffer_offset += 20;
	}
//	SentCount = XUartPs_Send(&Uart_PS, (u8 *) write_buffer, (u32) buffer_size);
//	if (SentCount != buffer_size) {
//		return XST_FAILURE;
//	}
	return SentCount;
}

void IPC_display_off() {
	char *displayOff = "\xFE\x53\x01";
	XUartPs_Send(&Uart_PS, (u8 *) displayOff, (u32) 3);
	usleep(100);  // sleep 1.5ms after clearing screen
//	*displayOff = "\xFE\x42";
//	XUartPs_Send(&Uart_PS, (u8 *) displayOff, (u32) 2);
//	usleep(100);  // sleep 1.5ms after clearing screen
}

void IPC_display_on() {
//	char *displayOn = "\xFE\x41";
//	XUartPs_Send(&Uart_PS, (u8 *) displayOn, (u32) 2);
//	usleep(100);  // sleep 1.5ms after clearing screen
	char *displayOn = "\xFE\x53\x08";
	XUartPs_Send(&Uart_PS, (u8 *) displayOn, (u32) 3);
	usleep(100);  // sleep 1.5ms after clearing screen
}

int IPC_UartPsLCDinit(u16 DeviceID){
	int Status;
	XUartPs_Config *Config;

	/*
	 * Initialize the UART driver so that it's ready to use
	 * Look up the configuration in the config table and then initialize it.
	 */
	Config = XUartPs_LookupConfig(DeviceID);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XUartPs_CfgInitialize(&Uart_PS, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XUartPs_SetBaudRate(&Uart_PS, 9600);

	/* Check hardware build. */
	Status = XUartPs_SelfTest(&Uart_PS);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;

}

