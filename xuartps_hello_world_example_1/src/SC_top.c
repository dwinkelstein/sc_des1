/*
 * SC_top.c
 *
 *  Created on: Feb 22, 2021
 *      Author: DanWinkelstein
 */

#include "SC_des1.h"

// #defines

#define EIGHT_SECONDS_DATA 9437184
#define NUMBUFFERS 3
 // Global definitions


// global interrupt event registers and flags
VOLT_INTERRUPT_T voltage_interrupt;
FREQ_INTERRUPT_T frequency_interrupt;
KEYPAD_INTERRUPT_T keypad_interrupt;
CURRENT_INTERRUPT_T current_interrupt;
STREAM_INTERRUPT_T stream_interrupt;
uint32_t receive_data_fifo_occupancy;
uint32_t stream_interrupt_status_reg;
uint32_t fabric_interrupt_event;

volatile u32 realTimeDataA[EIGHT_SECONDS_DATA] __attribute__ ((aligned (64)));
volatile u32 realTimeDataB[EIGHT_SECONDS_DATA] __attribute__ ((aligned (64)));
volatile u32 realTimeDataC[EIGHT_SECONDS_DATA] __attribute__ ((aligned (64)));
int currentBuffer = 0;
int currentBufferPtr = 0;

// this is the complete register map for the hardware
SC_DES1_T sc_des1_inst;   //out global register definition file
// create memory space for each of the individual register spaces
SNAPSHOT_MEMORY_T snapshot_data;
ALARM_REGISTER_T alarm_data;
SENSOR_FACTOR_T sensor_factor;
PCA9539_T pca9539;

ALARM_LIMIT_T alarm_limit_value; // this holds the alarm limits we plan to install

CONTACTOR_DISENGAGE_T contactor_setting;
extern XZDma ZDma;		/**<Instance of the ZDMA Device */
extern XScuGic xInterruptController;	/**< XIntc Instance */
extern XScuGic XZDmaIntc;



// function prototypes

int copyRealTimeData();
void dummy_set_sensor_factor();
void dummy_set_alarm_limits(ALARM_LIMIT_T *alarm_limit_value);   // remove or rename when we get real limits

void voltage_event_clear_operation();
void voltage_event_set_operation();
void frequency_event_clear_operation();
void frequency_event_set_operation();
void current_event_clear_operation();
void current_event_set_operation();
void keypad_event_operation();
void stream_snapshot_event_operation();

// debug
void pio_voltage();
void xdma_test();
int sc_top();
// main

#ifdef SNAPSHOT
int main() {
	sc_top();
}
#endif

#ifdef IPCTEST
int main(){
	char write_buffer[80];

	for(int i= 0; i< 10; i++){
                            //01234567890123456789012345678901234567890123456789012345678901234567890123456789

		sprintf(write_buffer,"Line 1 count %d     Line 2              Hello world         IPC TEST            ",i);
		clear_LCD();
		LCD_output(write_buffer, 80);
		sleep(5);
	}

}
#endif

int sc_top() {

//    init_platform();


	sc_des1_inst.snapshot_data = &snapshot_data;
	sc_des1_inst.alarm_data = &alarm_data;
	sc_des1_inst.sensor_factor = &sensor_factor;
	sc_des1_inst.temperature = 0;
	sc_des1_inst.pca9539 = &pca9539;
	CONTROL_REG_T local_control_reg;
	local_control_reg.reg = 0;
	set_control_reg(&sc_des1_inst, &local_control_reg);
	CONTACTOR_DISENGAGE_T local_contactor_control;
	local_contactor_control.reg = 0;
	set_contactor(local_contactor_control, &sc_des1_inst);

#ifndef IPC_DISPLAY
	// initialize LCD display UART
	UartPsLCDinit(LCD_UART_DEVICE_ID);
#endif
	// initialize I2C system
	SC_I2C_Init(IIC_DEVICE_ID);
	// configure the PCA9539 as input on both ports
	config_pca9539(PCA9539_INPUT,PCA9539_INPUT);
	// Initialize the interrupt subsystem
	fabric_interrupt_init();
	// initialize XZDMA system
	XZDma_Init(&ZDma, ZDMA_DEVICE_ID);
	// Connect to the interrupt controller.
	SetupXZDmaInterruptSystem_example(&xInterruptController, &ZDma,  ZDMA_INTR_DEVICE_ID);
    //set the system frequency to 60Hz to start with
	set_system_frequency(F60HZ, &sc_des1_inst);
	Xil_In32(ALARM_MEMORY_ADDR);
	// set the voltage and current factors
	dummy_set_sensor_factor();
	// set the alarm limits
	dummy_set_alarm_limits(&alarm_limit_value);   // remove or rename when we get real limits
	set_alarm_limits(&sc_des1_inst, &alarm_limit_value);
	// write the function select LED screen
	display_function_select();
	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;  // no keys
	sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;  //B key puts statemachine in\\\\\ function screen
	standalone_display(&sc_des1_inst);
	// initialize pingpong buffers
	currentBuffer = 0;
	currentBufferPtr = 0;

	// set the timestamp based on time-of-day (or explicitly if bare-metal)
#ifdef PETALINUX
	set_timestamp(sc_des1_inst);
#else
	set_timestamp_direct(&sc_des1_inst, 0,0);
#endif
	contactor_setting.reg = 0;  // all contactors engaged (no shedding)
	set_contactor(contactor_setting, &sc_des1_inst);

	fabric_interrupt_event = 0;

/************************************************
 *
 * PIO operation test of hardware
 */
//	pio_voltage();
//	xdma_test();

	// hit start
	local_control_reg.reg = 0;
	local_control_reg.bits.dataCollectEnable = 1;
	local_control_reg.bits.streamCollectEnable = 0;
	local_control_reg.bits.greenLEDEnable = 1;
	local_control_reg.bits.interruptEnable = 0;
	local_control_reg.bits.streamIntEnable = 0;
	local_control_reg.bits.snapShotIntEnable = 0;
	local_control_reg.bits.alarmInterruptEnable = 0;  // disable alarm interrupts (and keypad)
	local_control_reg.bits.freq60_50 = F60HZ;
	set_control_reg(&sc_des1_inst, &local_control_reg);

	//enable interrupts in the AXIS/AXI fifo
	Xil_Out32(FIFO_REG_SRR_ADDR,0x000000A5); // reset the Stream FIFO
	Xil_Out32(FIFO_REG_IER_ADDR, 0x00100000); // Set the IER for Recieve Fifo Programmable Full (PG080 page 22)
	Xil_Out32(FIFO_REG_ISR_ADDR, 0xFFF80000); // Clear the ISR register (PG080 page 22)
	local_control_reg.bits.streamIntEnable = 1;     /// enable stream interrupts
	local_control_reg.bits.streamCollectEnable = 1;
	local_control_reg.bits.interruptEnable = 1;
	local_control_reg.bits.snapShotIntEnable = 1;
	local_control_reg.bits.alarmInterruptEnable = 1;
	set_control_reg(&sc_des1_inst, &local_control_reg);


	while(1){  // forever run standalone design

		while(fabric_interrupt_event == 0);  // wait for interrupt to wake us up
		// reset the interrupt flag
		fabric_interrupt_event = 0;
//#ifdef ALL_ALARMS
		if(voltage_interrupt.reg != 0 || current_interrupt.reg != 0 || frequency_interrupt.reg != 0){
			get_alarm_data(&sc_des1_inst);
		}
// voltage/circuit breaker/contactor interrupt [NOT sure what to do, print out results go to Voltage display]
		if((sc_des1_inst.alarm_data->volt_interrupt_reg.reg & ~voltage_interrupt.reg) != 0) {  // Interrupt clear for frequency/phase
			voltage_event_clear_operation();

		} else if((~sc_des1_inst.alarm_data->volt_interrupt_reg.reg & voltage_interrupt.reg) != 0) {  //Interrupt SET event voltage
			voltage_event_set_operation();
		}
// frequency/phase interrupt  [NOT SURE WHAT TO DO, just printing the results and goto the LCD display]
		if((sc_des1_inst.alarm_data->freq_interrupt_reg.reg & ~frequency_interrupt.reg) != 0) {  // Interrupt clear for frequency/phase
			frequency_event_clear_operation();

		} else if((~sc_des1_inst.alarm_data->freq_interrupt_reg.reg & frequency_interrupt.reg) != 0) {  //Interrupt SET event frequency phase
			frequency_event_set_operation();
		}
			// current interrupt
		//**********NOTE*****************
		// In the event of a current interrupt on any downstream feed, we automatically disengage the contactor
		// for that downstream feed
		if((sc_des1_inst.alarm_data->current_interrupt_reg.reg & ~current_interrupt.reg) != 0) {  // Interrupt clear
			current_event_clear_operation();

		} else if((~sc_des1_inst.alarm_data->current_interrupt_reg.reg & current_interrupt.reg) != 0) {  //Interrupt SET event
			current_event_set_operation();
		}
//#endif
// keypad interupt
		if(keypad_interrupt.reg != 0){
			keypad_event_operation();

		}
// stream interrupt
		if(stream_interrupt.reg != 0){
			stream_snapshot_event_operation();
		}
		// update the sc_des1_inst
		get_interrupt_status(&sc_des1_inst);

// re-enable interrupts
		local_control_reg.reg = Xil_In32(CONTROL_REG_ADDR);
		local_control_reg.bits.interruptEnable = 1;
		set_control_reg(&sc_des1_inst, &local_control_reg);

	}
//	close_platform();
	return 0;
}

extern XZDma ZDma;		/**<Instance of the ZDMA Device */
extern INTC Intc;	/**< XIntc Instance */

int wordsInFifo;
unsigned long long copyRealTimeDataCount = 0;

int copyRealTimeData(){
// make sure we have at least 512 words in the hardware buffer
	copyRealTimeDataCount++;
	wordsInFifo = Xil_In32(FIFO_REG_RDFO_ADDR);
//	while(wordsInFifo > TRANSFERSIZE *2 + 36){  // linear mode
	while(wordsInFifo > TRANSFERSIZE + 36){
		switch(currentBuffer) {
		case 0:
			currentBufferPtr += XDMA_Transfer(&ZDma, &realTimeDataA[currentBufferPtr]);
	//		currentBufferPtr += XDMA_Tranfer_example(&ZDma,&realTimeDataA[currentBufferPtr],currentBufferPtr);
			break;
		case 1:
			currentBufferPtr += XDMA_Transfer(&ZDma, &realTimeDataB[currentBufferPtr]);
	//		currentBufferPtr += XDMA_Tranfer_example(&ZDma,&realTimeDataB[currentBufferPtr],currentBufferPtr);
			break;
		case 2:
			currentBufferPtr += XDMA_Transfer(&ZDma, &realTimeDataC[currentBufferPtr]);
	//		currentBufferPtr += XDMA_Tranfer_example(&ZDma,&realTimeDataC[currentBufferPtr],currentBufferPtr);
			break;
		}
		if(currentBufferPtr >= EIGHT_SECONDS_DATA){
			if(++currentBuffer >= NUMBUFFERS) {
				currentBuffer = 0;
			}
			currentBufferPtr = 0;
		}
		wordsInFifo = Xil_In32(FIFO_REG_RDFO_ADDR);
	}


	return XST_SUCCESS;
}
	//This is a dummy function to set the sensor voltage and current factors.
	//This will be replaced by (likely) a read from a file that is IPDU specific
	//or (unlikely) a set of values that are universal
void dummy_set_sensor_factor() {
	sc_des1_inst.sensor_factor->volt_factor_l1 = 57.9; // assume 120(RMS) peak = 169V reads 16384 (half deflection) of ADC
	sc_des1_inst.sensor_factor->volt_factor_l2 = 58.1; // assume 120(RMS) peak = 169V reads 16384 (half deflection) of ADC
	sc_des1_inst.sensor_factor->volt_factor_l3 = 61.1; // assume 120(RMS) peak = 169V reads 16384 (half deflection) of ADC
	sc_des1_inst.sensor_factor->current_main_factor_l1 = 2145.67; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_main_factor_l2 = 2194.26; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_main_factor_l3 = 2227.51; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_main_factor_n = 2264.06; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_BYP_factor_l1 = 115.8524; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_BYP_factor_l2 = 115.8524; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_BYP_factor_l3 = 115.8524; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_BYP_factor_n = 115.8524; // assume 100A(RMS) peak = 141A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60A_factor_l1 = 193.0872; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60A_factor_l2 = 193.0872; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60A_factor_l3 = 193.0872; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60A_factor_n = 193.0872; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60B_factor_l1 = 367; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60B_factor_l2 = 312; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60B_factor_l3 = 292; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_60B_factor_n = 282; // assume 60A(RMS) peak = 85A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40A_factor_l1 = 289.6309; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40A_factor_l2 = 289.6309; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40A_factor_l3 = 289.6309; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40A_factor_n = 289.6309; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40B_factor_l1 = 289.6309; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40B_factor_l2 = 289.6309; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40B_factor_l3 = 289.6309; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_40B_factor_n = 289.6309; // assume 40A(RMS) peak = 57A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_20A_factor_l1 = 579.2619; // assume 20A(RMS) peak = 28A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_20A_factor_n = 579.2619; // assume 20A(RMS) peak = 28A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_20B_factor_l2 = 579.2619; // assume 20A(RMS) peak = 28A reads 16384 (half deflection of ADC
	sc_des1_inst.sensor_factor->current_20B_factor_n = 579.2619; // assume 20A(RMS) peak = 28A reads 16384 (half deflection of ADC

	}

void dummy_set_alarm_limits(ALARM_LIMIT_T *alarm_limit_value){
	alarm_limit_value->main_l1_volt_max_reg = 140.0 * sc_des1_inst.sensor_factor->volt_factor_l1;  //140V max
	alarm_limit_value->main_l1_volt_min_reg = 96.0 * sc_des1_inst.sensor_factor->volt_factor_l1;  //960V max
	alarm_limit_value->main_l2_volt_max_reg = 140.0 * sc_des1_inst.sensor_factor->volt_factor_l2;  //140V max
	alarm_limit_value->main_l2_volt_min_reg = 96.0 * sc_des1_inst.sensor_factor->volt_factor_l2;  //960V max
	alarm_limit_value->main_l3_volt_max_reg = 140.0 * sc_des1_inst.sensor_factor->volt_factor_l3;  //140V max
	alarm_limit_value->main_l3_volt_min_reg = 96.0 * sc_des1_inst.sensor_factor->volt_factor_l3;  //960V max

	get_control_reg(&sc_des1_inst);

	if(sc_des1_inst.alarm_data->control_reg.bits.freq60_50 == F60HZ){  // 60Hz
		alarm_limit_value->main_freq_max_reg = 0xFFFFFFFF;
		alarm_limit_value->main_freq_min_reg = 0;
	} else {
		alarm_limit_value->main_freq_max_reg = 0xFFFFFFFF;
		alarm_limit_value->main_freq_min_reg = 0;

	}
	if(sc_des1_inst.alarm_data->control_reg.bits.freq60_50 == F60HZ){  // 60Hz
		alarm_limit_value->main_phase_max_reg = 0xFFFFFFFF;
		alarm_limit_value->main_phase_min_reg = 0;
	} else {
		alarm_limit_value->main_phase_max_reg = 0xFFFFFFFF;
		alarm_limit_value->main_phase_min_reg = 0;

	}
	      alarm_limit_value->main_l1_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->main_l2_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->main_l3_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->main_n_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f60a_l1_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f60a_l2_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f60a_l3_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f60a_n_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f60b_l1_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f60b_l2_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f60b_l3_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f60b_n_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f40a_l1_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f40a_l2_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f40a_l3_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f40a_n_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f40b_l1_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f40b_l2_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f40b_l3_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f40b_n_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f20a_l1_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f20a_n_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f20b_l2_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->f20b_n_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->fbyp_l1_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->fbyp_l2_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->fbyp_l3_amp_max_reg = 0x3596F593; //~600%
	      alarm_limit_value->fbyp_n_amp_max_reg = 0x3596F593; //~600%


}

void voltage_event_clear_operation(){
	printf("Voltage Interrupt clear event %08x\n", sc_des1_inst.alarm_data->volt_interrupt_reg.reg & ~voltage_interrupt.reg);
// goto Voltage Snapshot display on the LCD display
	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
	standalone_display(&sc_des1_inst);
	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	sc_des1_inst.alarm_data->keypad_value_reg.bits.key1Value = 1;
	standalone_display(&sc_des1_inst);
}

void voltage_event_set_operation(){
	printf("VOLTAGE Interrupt set event \n");
// goto Voltage Snapshot display on the LCD display
	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
	standalone_display(&sc_des1_inst);
	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	sc_des1_inst.alarm_data->keypad_value_reg.bits.key3Value = 1;
	standalone_display(&sc_des1_inst);
}

void frequency_event_clear_operation(){
	printf("FREQUENCY/PHASE Interrupt clear event %08x\n", sc_des1_inst.alarm_data->freq_interrupt_reg.reg & ~frequency_interrupt.reg);
	// goto Frequency Snapshot display on the LCD display
	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
	standalone_display(&sc_des1_inst);
	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	sc_des1_inst.alarm_data->keypad_value_reg.bits.key3Value = 1;
	standalone_display(&sc_des1_inst);
}

void frequency_event_set_operation(){
	printf("FREQUENCY/PHASE Interrupt set event %08x\n", ~sc_des1_inst.alarm_data->freq_interrupt_reg.reg & frequency_interrupt.reg);
	// goto Frequency Snapshot display on the LCD display
	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	sc_des1_inst.alarm_data->keypad_value_reg.bits.keyBValue = 1;
	standalone_display(&sc_des1_inst);
	sc_des1_inst.alarm_data->keypad_value_reg.reg = 0;
	sc_des1_inst.alarm_data->keypad_value_reg.bits.key3Value = 1;
	standalone_display(&sc_des1_inst);
}

void current_event_clear_operation(){
	printf("CURRENT Interrupt clear event %08x\n", sc_des1_inst.alarm_data->current_interrupt_reg.reg & ~current_interrupt.reg);
}

//**********NOTE*****************
// In the event of a current interrupt on any downstream feed, we automatically disengage the contactor
// for that downstream feed

void current_event_set_operation(){
	printf("CURRENT Interrupt set event %08x\n", ~sc_des1_inst.alarm_data->current_interrupt_reg.reg & current_interrupt.reg);
	CONTACTOR_DISENGAGE_T currentContactor;
	currentContactor.reg = sc_des1_inst.alarm_data->contactor_disengage_reg.reg;
	// bypass alarm
	if(current_interrupt.bits.byp_L1_Overcurrent == 1 ||
			current_interrupt.bits.byp_L2_Overcurrent == 1 ||
			current_interrupt.bits.byp_L3_Overcurrent == 1 ||
			current_interrupt.bits.byp_N_Overcurrent == 1) {
		printf("BYPASS Overcurrent Alarm:  Shedding feed\n");
		CONTACTOR_DISENGAGE_T currentContactor;
		currentContactor.reg = sc_des1_inst.alarm_data->contactor_disengage_reg.reg;
		currentContactor.bits.bypassContactor = 1;
		set_contactor(currentContactor, &sc_des1_inst);
	}
	// 60A alarm
	if(current_interrupt.bits.f60A_L1_Overcurrent == 1 ||
				current_interrupt.bits.f60A_L2_Overcurrent == 1 ||
				current_interrupt.bits.f60A_L3_Overcurrent == 1 ||
				current_interrupt.bits.f60A_N_Overcurrent == 1) {
			printf("Feed 60A Overcurrent Alarm:  Shedding feed\n");
			currentContactor.bits.f60AContactor = 1;
			set_contactor(currentContactor, &sc_des1_inst);
	}
	// 60B alarm
	if(current_interrupt.bits.f60B_L1_Overcurrent == 1 ||
				current_interrupt.bits.f60B_L2_Overcurrent == 1 ||
				current_interrupt.bits.f60B_L3_Overcurrent == 1 ||
				current_interrupt.bits.f60B_N_Overcurrent == 1) {
			printf("Feed 60B Overcurrent Alarm:  Shedding feed\n");
			currentContactor.bits.f60BContactor = 1;
			set_contactor(currentContactor, &sc_des1_inst);
	}
	// 40A alarm
	if(current_interrupt.bits.f40A_L1_Overcurrent == 1 ||
				current_interrupt.bits.f40A_L2_Overcurrent == 1 ||
				current_interrupt.bits.f40A_L3_Overcurrent == 1 ||
				current_interrupt.bits.f40A_N_Overcurrent == 1) {
			printf("Feed 40A Overcurrent Alarm:  Shedding feed\n");
			currentContactor.bits.f40AContactor = 1;
			set_contactor(currentContactor, &sc_des1_inst);
	}
	// 40B alarm
	if(current_interrupt.bits.f40B_L1_Overcurrent == 1 ||
				current_interrupt.bits.f40B_L2_Overcurrent == 1 ||
				current_interrupt.bits.f40B_L3_Overcurrent == 1 ||
				current_interrupt.bits.f40B_N_Overcurrent == 1) {
			printf("Feed 40B Overcurrent Alarm:  Shedding feed\n");
			currentContactor.bits.f40BContactor = 1;
			set_contactor(currentContactor, &sc_des1_inst);
	}
	// 20A alarm
	if(current_interrupt.bits.f20A_L1_Overcurrent == 1 ||
				current_interrupt.bits.f20A_N_Overcurrent == 1) {
			printf("Feed 20A Overcurrent Alarm:  Shedding feed\n");
			currentContactor.bits.f20AContactor = 1;
			set_contactor(currentContactor, &sc_des1_inst);
	}
	// 20B alarm
	if(current_interrupt.bits.f20B_L2_Overcurrent == 1 ||
				current_interrupt.bits.f20B_N_Overcurrent == 1) {
			printf("Feed 20B Overcurrent Alarm:  Shedding feed\n");
			currentContactor.bits.f20BContactor = 1;
			set_contactor(currentContactor, &sc_des1_inst);
	}
}

void keypad_event_operation(){
		get_keypad_values(&sc_des1_inst);
		standalone_display(&sc_des1_inst);
}
int screen_update_interval = 0;

void stream_snapshot_event_operation(){
		if(stream_interrupt.bits.streamFifoInterrupt == 1) {
			copyRealTimeData();
			Xil_Out32(FIFO_REG_ISR_ADDR, 0xFFF80000); // Clear the ISR register (PG080 page 22)
			Xil_Out32(FIFO_REG_IER_ADDR, 0x00100000); // Set the IER for Recieve Fifo Programmable Full (PG080 page 22)
		}
		else if(stream_interrupt.bits.snapShotUpdateInterrupt == 1) {
			get_snapshot_data(&sc_des1_inst);
			get_temperature(&sc_des1_inst);
//			get_pca9539_status(&sc_des1_inst);  // DOES NOT WORK DUE TO NO I2C RESTART
			get_alarm_data(&sc_des1_inst);
			screen_update_interval++;
			if(screen_update_interval > 4){
				update_LCD_display(&sc_des1_inst);
				screen_update_interval = 0;
			}
		}
}

