/*
 * SC_PIO.c
 *
 *  Created on: Feb 17, 2021
 *      Author: DanWinkelstein
 */

#include "SC_des1.h"

extern VOLT_INTERRUPT_T voltage_interrupt;
extern FREQ_INTERRUPT_T frequency_interrupt;
extern KEYPAD_INTERRUPT_T keypad_interrupt;
extern CURRENT_INTERRUPT_T current_interrupt;
extern STREAM_INTERRUPT_T stream_interrupt;


// copy everyting in the snapshot memory into the sc_des1_inst instance
// uses Memcpy as a PIO operation.  (if this takes too long we will do DMA)
void get_snapshot_data(SC_DES1_T *sc_des1_inst) {  // get all the data in the snapshot memory and put it into the datastructure
//	Xil_MemCpy(sc_des1_inst->snapshot_data, SNAPSHOT_MEMORY_ADDR, SNAPSHOT_MEMORY_SIZE);
	sc_des1_inst->snapshot_data->snapshot_volt_l1 = Xil_In32(SNAPSHOT_VOLT_L1_ADDR);
	sc_des1_inst->snapshot_data->snapshot_volt_l2 = Xil_In32(SNAPSHOT_VOLT_L2_ADDR);
	sc_des1_inst->snapshot_data->snapshot_volt_l3 = Xil_In32(SNAPSHOT_VOLT_L3_ADDR);
	sc_des1_inst->snapshot_data->snapshot_noise = Xil_In32(SNAPSHOT_NOISE_ADDR);

	sc_des1_inst->snapshot_data->snapshot_amp_main_l1 = Xil_In32(SNAPSHOT_AMP_MAIN_L1);
	sc_des1_inst->snapshot_data->snapshot_amp_main_l2 = Xil_In32(SNAPSHOT_AMP_MAIN_L2);
	sc_des1_inst->snapshot_data->snapshot_amp_main_l3 = Xil_In32(SNAPSHOT_AMP_MAIN_L3);
	sc_des1_inst->snapshot_data->snapshot_amp_main_n = Xil_In32(SNAPSHOT_AMP_MAIN_N);
	sc_des1_inst->snapshot_data->snapshot_pow_main_l1 = Xil_In32(SNAPSHOT_POW_MAIN_L1);
	sc_des1_inst->snapshot_data->snapshot_pow_main_l2 = Xil_In32(SNAPSHOT_POW_MAIN_L2);
	sc_des1_inst->snapshot_data->snapshot_pow_main_l3 = Xil_In32(SNAPSHOT_POW_MAIN_L3);

	sc_des1_inst->snapshot_data->snapshot_amp_f60a_l1 = Xil_In32(SNAPSHOT_AMP_F60A_L1);
	sc_des1_inst->snapshot_data->snapshot_amp_f60a_l2 = Xil_In32(SNAPSHOT_AMP_F60A_L2);
	sc_des1_inst->snapshot_data->snapshot_amp_f60a_l3 = Xil_In32(SNAPSHOT_AMP_F60A_L3);
	sc_des1_inst->snapshot_data->snapshot_amp_f60a_n = Xil_In32(SNAPSHOT_AMP_F60A_N);
	sc_des1_inst->snapshot_data->snapshot_pow_f60a_l1 = Xil_In32(SNAPSHOT_POW_F60A_L1);
	sc_des1_inst->snapshot_data->snapshot_pow_f60a_l2 = Xil_In32(SNAPSHOT_POW_F60A_L2);
	sc_des1_inst->snapshot_data->snapshot_pow_f60a_l3 = Xil_In32(SNAPSHOT_POW_F60A_L3);

	sc_des1_inst->snapshot_data->snapshot_amp_f60b_l1 = Xil_In32(SNAPSHOT_AMP_F60B_L1);
	sc_des1_inst->snapshot_data->snapshot_amp_f60b_l2 = Xil_In32(SNAPSHOT_AMP_F60B_L2);
	sc_des1_inst->snapshot_data->snapshot_amp_f60b_l3 = Xil_In32(SNAPSHOT_AMP_F60B_L3);
	sc_des1_inst->snapshot_data->snapshot_amp_f60b_n = Xil_In32(SNAPSHOT_AMP_F60B_N);
	sc_des1_inst->snapshot_data->snapshot_pow_f60b_l1 = Xil_In32(SNAPSHOT_POW_F60B_L1);
	sc_des1_inst->snapshot_data->snapshot_pow_f60b_l2 = Xil_In32(SNAPSHOT_POW_F60B_L2);
	sc_des1_inst->snapshot_data->snapshot_pow_f60b_l3 = Xil_In32(SNAPSHOT_POW_F60B_L3);

	sc_des1_inst->snapshot_data->snapshot_amp_f40a_l1 = Xil_In32(SNAPSHOT_AMP_F40A_L1);
	sc_des1_inst->snapshot_data->snapshot_amp_f40a_l2 = Xil_In32(SNAPSHOT_AMP_F40A_L2);
	sc_des1_inst->snapshot_data->snapshot_amp_f40a_l3 = Xil_In32(SNAPSHOT_AMP_F40A_L3);
	sc_des1_inst->snapshot_data->snapshot_amp_f40a_n = Xil_In32(SNAPSHOT_AMP_F40A_N);
	sc_des1_inst->snapshot_data->snapshot_pow_f40a_l1 = Xil_In32(SNAPSHOT_POW_F40A_L1);
	sc_des1_inst->snapshot_data->snapshot_pow_f40a_l2 = Xil_In32(SNAPSHOT_POW_F40A_L2);
	sc_des1_inst->snapshot_data->snapshot_pow_f40a_l3 = Xil_In32(SNAPSHOT_POW_F40A_L3);

	sc_des1_inst->snapshot_data->snapshot_amp_f40b_l1 = Xil_In32(SNAPSHOT_AMP_F40B_L1);
	sc_des1_inst->snapshot_data->snapshot_amp_f40b_l2 = Xil_In32(SNAPSHOT_AMP_F40B_L2);
	sc_des1_inst->snapshot_data->snapshot_amp_f40b_l3 = Xil_In32(SNAPSHOT_AMP_F40B_L3);
	sc_des1_inst->snapshot_data->snapshot_amp_f40b_n = Xil_In32(SNAPSHOT_AMP_F40B_N);
	sc_des1_inst->snapshot_data->snapshot_pow_f40b_l1 = Xil_In32(SNAPSHOT_POW_F40B_L1);
	sc_des1_inst->snapshot_data->snapshot_pow_f40b_l2 = Xil_In32(SNAPSHOT_POW_F40B_L2);
	sc_des1_inst->snapshot_data->snapshot_pow_f40b_l3 = Xil_In32(SNAPSHOT_POW_F40B_L3);

	sc_des1_inst->snapshot_data->snapshot_amp_f20a_l1 = Xil_In32(SNAPSHOT_AMP_F20A_L1);
	sc_des1_inst->snapshot_data->snapshot_amp_f20a_n = Xil_In32(SNAPSHOT_AMP_F20A_N);
	sc_des1_inst->snapshot_data->snapshot_pow_f20a_l1 = Xil_In32(SNAPSHOT_POW_F20A_L1);

	sc_des1_inst->snapshot_data->snapshot_amp_f20b_l2 = Xil_In32(SNAPSHOT_AMP_F20B_L2);
	sc_des1_inst->snapshot_data->snapshot_amp_f20b_n = Xil_In32(SNAPSHOT_AMP_F20B_N);
	sc_des1_inst->snapshot_data->snapshot_pow_f20b_l2 = Xil_In32(SNAPSHOT_POW_F20B_L2);

	sc_des1_inst->snapshot_data->snapshot_amp_byp_l1 = Xil_In32(SNAPSHOT_AMP_FBYP_L1);
	sc_des1_inst->snapshot_data->snapshot_amp_byp_l2 = Xil_In32(SNAPSHOT_AMP_FBYP_L2);
	sc_des1_inst->snapshot_data->snapshot_amp_byp_l3 = Xil_In32(SNAPSHOT_AMP_FBYP_L3);
	sc_des1_inst->snapshot_data->snapshot_amp_byp_n = Xil_In32(SNAPSHOT_AMP_FBYP_N);
	sc_des1_inst->snapshot_data->snapshot_pow_byp_l1 = Xil_In32(SNAPSHOT_POW_FBYP_L1);
	sc_des1_inst->snapshot_data->snapshot_pow_byp_l2 = Xil_In32(SNAPSHOT_POW_FBYP_L2);
	sc_des1_inst->snapshot_data->snapshot_pow_byp_l3 = Xil_In32(SNAPSHOT_POW_FBYP_L3);

	return XST_SUCCESS;
}

// copy all the alarm registers into the sc_des1_inst instance
// this is from contactor disengage to fbyps_n_amp_reg inclusive [FIXME, needs to be a usable structure]
// uses Memcpy as a PIO operation on 39 words
void get_alarm_data(SC_DES1_T *sc_des1_inst) {  // get all the data in the snapshot memory and put it into the datastructure
	Xil_MemCpy(&sc_des1_inst->alarm_data->contactor_disengage_reg, CONTACTOR_DISENGAGE_ADDR, ALARM_DATA_SIZE);

}

// Alarm limits are not likely to be changed often, so we setup a structure with all of the limits and load them at once
// we can't use MemCpy as it causes an overflow of the AXI bus in the FPGA.
void set_alarm_limits(SC_DES1_T *sc_des1_inst, ALARM_LIMIT_T *alarm_limits_value){ //alarm limits populated in
	Xil_MemCpy(MAIN_L1_VOLT_MAX_ADDR, &alarm_limits_value->main_l1_volt_max_reg, sizeof(ALARM_LIMIT_T));
//	Xil_Out32(MAIN_L1_VOLT_MAX_ADDR,alarm_limits_value->main_l1_volt_max_reg);
//	Xil_Out32(MAIN_L1_VOLT_MIN_ADDR,alarm_limits_value->main_l1_volt_min_reg);
//	Xil_Out32(MAIN_L2_VOLT_MAX_ADDR,alarm_limits_value->main_l2_volt_max_reg);
//	Xil_Out32(MAIN_L2_VOLT_MIN_ADDR,alarm_limits_value->main_l2_volt_min_reg);
//	Xil_Out32(MAIN_L3_VOLT_MAX_ADDR,alarm_limits_value->main_l3_volt_max_reg);
//	Xil_Out32(MAIN_L3_VOLT_MIN_ADDR,alarm_limits_value->main_l3_volt_min_reg);
//
//	Xil_Out32(MAIN_FREQ_MAX_ADDR,alarm_limits_value->main_freq_max_reg);
//	Xil_Out32(MAIN_FREQ_MIN_ADDR,alarm_limits_value->main_freq_min_reg);
//	Xil_Out32(MAIN_PHASE_MAX_ADDR,alarm_limits_value->main_phase_max_reg);
//	Xil_Out32(MAIN_PHASE_MIN_ADDR,alarm_limits_value->main_phase_min_reg);
//
//	Xil_Out32(MAIN_L1_AMP_MAX_ADDR,alarm_limits_value->main_l1_amp_max_reg);
//	Xil_Out32(MAIN_L2_AMP_MAX_ADDR,alarm_limits_value->main_l2_amp_max_reg);
//	Xil_Out32(MAIN_L3_AMP_MAX_ADDR,alarm_limits_value->main_l3_amp_max_reg);
//	Xil_Out32(MAIN_N_AMP_MAX_ADDR,alarm_limits_value->main_n_amp_max_reg);
//
//	Xil_Out32(F60A_L1_AMP_MAX_ADDR,alarm_limits_value->f60a_l1_amp_max_reg);
//	Xil_Out32(F60A_L2_AMP_MAX_ADDR,alarm_limits_value->f60a_l2_amp_max_reg);
//	Xil_Out32(F60A_L3_AMP_MAX_ADDR,alarm_limits_value->f60a_l3_amp_max_reg);
//	Xil_Out32(F60A_N_AMP_MAX_ADDR,alarm_limits_value->f60a_n_amp_max_reg);
//
//	Xil_Out32(F60B_L1_AMP_MAX_ADDR,alarm_limits_value->f60b_l1_amp_max_reg);
//	Xil_Out32(F60B_L2_AMP_MAX_ADDR,alarm_limits_value->f60b_l2_amp_max_reg);
//	Xil_Out32(F60B_L3_AMP_MAX_ADDR,alarm_limits_value->f60b_l3_amp_max_reg);
//	Xil_Out32(F60B_N_AMP_MAX_ADDR,alarm_limits_value->f60b_n_amp_max_reg);
//
//	Xil_Out32(F40A_L1_AMP_MAX_ADDR,alarm_limits_value->f40a_l1_amp_max_reg);
//	Xil_Out32(F40A_L2_AMP_MAX_ADDR,alarm_limits_value->f40a_l2_amp_max_reg);
//	Xil_Out32(F40A_L3_AMP_MAX_ADDR,alarm_limits_value->f40a_l3_amp_max_reg);
//	Xil_Out32(F40A_N_AMP_MAX_ADDR,alarm_limits_value->f40a_n_amp_max_reg);
//
//	Xil_Out32(F40B_L1_AMP_MAX_ADDR,alarm_limits_value->f40b_l1_amp_max_reg);
//	Xil_Out32(F40B_L2_AMP_MAX_ADDR,alarm_limits_value->f40b_l2_amp_max_reg);
//	Xil_Out32(F40B_L3_AMP_MAX_ADDR,alarm_limits_value->f40b_l3_amp_max_reg);
//	Xil_Out32(F40B_N_AMP_MAX_ADDR,alarm_limits_value->f40b_n_amp_max_reg);
//
//	Xil_Out32(F20A_L1_AMP_MAX_ADDR,alarm_limits_value->f20a_l1_amp_max_reg);
//	Xil_Out32(F20A_N_AMP_MAX_ADDR,alarm_limits_value->f20a_n_amp_max_reg);
//	Xil_Out32(F20B_L2_AMP_MAX_ADDR,alarm_limits_value->f20b_l2_amp_max_reg);
//	Xil_Out32(F20B_N_AMP_MAX_ADDR,alarm_limits_value->f20b_n_amp_max_reg);
//
//	Xil_Out32(FBYP_L1_AMP_MAX_ADDR,alarm_limits_value->fbyp_l1_amp_max_reg);
//	Xil_Out32(FBYP_L2_AMP_MAX_ADDR,alarm_limits_value->fbyp_l2_amp_max_reg);
//	Xil_Out32(FBYP_L3_AMP_MAX_ADDR,alarm_limits_value->fbyp_l3_amp_max_reg);
//	Xil_Out32(FBYP_N_AMP_MAX_ADDR,alarm_limits_value->fbyp_n_amp_max_reg);




	Xil_MemCpy(&sc_des1_inst->alarm_data->main_l1_volt_max_reg,alarm_limits_value,sizeof(ALARM_LIMIT_T));
	return XST_SUCCESS;
}


// Set the timestamp based on the system time (normally used by default)
void set_timestamp(SC_DES1_T *sc_des1_inst){
	struct timeval tv;
#ifdef PETALINUX
	gettimeofday(&tv, NULL);
#else
	tv.tv_sec = 0;
	tv.tv_usec = 0;
#endif
	u64 seconds = (u64)tv.tv_sec;
	u32 nanoseconds = tv.tv_usec*1000;
	set_timestamp_direct(sc_des1_inst, seconds, nanoseconds);
}

// explicitly set the timestamp based on seconds/nanoseconds
void set_timestamp_direct(SC_DES1_T *sc_des1_inst, u64 seconds, u32 nanoseconds) {
	Xil_Out32(TIMESTAMP_SECOND_MSB_ADDR, seconds >> 32);
	Xil_Out32(TIMESTAMP_SECOND_LSB_ADDR, seconds & 0xFFFFFFFF);
	Xil_Out32(TIMESTAMP_NANOSECOND_ADDR, nanoseconds);

	sc_des1_inst->alarm_data->timestamp_seconds_msb = seconds >> 32;
	sc_des1_inst->alarm_data->timestamp_seconds_lsb = seconds & 0xffffffff;
	sc_des1_inst->alarm_data->timestamp_nanoseconds = nanoseconds;
}


// Interrupt status is obtained in the interrupt service routine only.  Move data to shadow registers
void get_interrupt_status(SC_DES1_T *sc_des1_inst){
	sc_des1_inst->alarm_data->volt_interrupt_reg.reg = voltage_interrupt.reg;
	sc_des1_inst->alarm_data->freq_interrupt_reg.reg = frequency_interrupt.reg;
	sc_des1_inst->alarm_data->keypad_interrupt_reg.reg = keypad_interrupt.reg;
	sc_des1_inst->alarm_data->current_interrupt_reg.reg = current_interrupt.reg;
	sc_des1_inst->alarm_data->stream_interrupt_reg.reg = stream_interrupt.reg;
}

void set_control_reg(SC_DES1_T *sc_des1_inst, CONTROL_REG_T *control_reg_value) {
	Xil_Out32(CONTROL_REG_ADDR, control_reg_value->reg);
	sc_des1_inst->alarm_data->control_reg.reg = control_reg_value->reg;
}

void get_control_reg(SC_DES1_T *sc_des1_inst) {
	CONTROL_REG_T control_reg_value;
	control_reg_value.reg = Xil_In32(CONTROL_REG_ADDR);
	sc_des1_inst->alarm_data->control_reg.reg = control_reg_value.reg;
}


void get_contactor_reg(SC_DES1_T *sc_des1_inst) {
	sc_des1_inst->alarm_data->contactor_disengage_reg.reg = Xil_In32(CONTACTOR_DISENGAGE_ADDR);
}

void get_circuitbreaker_reg(SC_DES1_T *sc_des1_inst){
	sc_des1_inst->alarm_data->circuit_breaker_status_reg.reg = Xil_In32(CIRCUIT_BREAKER_STATUS_ADDR);
}

void get_timestamp(SC_DES1_T *sc_des1_inst){
	sc_des1_inst->alarm_data->timestamp_seconds_msb = Xil_In32(TIMESTAMP_SECOND_MSB_ADDR);
	sc_des1_inst->alarm_data->timestamp_seconds_lsb = Xil_In32(TIMESTAMP_SECOND_LSB_ADDR);
	sc_des1_inst->alarm_data->timestamp_nanoseconds = Xil_In32(TIMESTAMP_NANOSECOND_ADDR);
}

void get_keypad_values(SC_DES1_T *sc_des1_inst){
	sc_des1_inst->alarm_data->keypad_value_reg.reg = Xil_In32(KEYPAD_VALUE_ADDR);
}

void set_contactor(CONTACTOR_DISENGAGE_T contactor_setting, SC_DES1_T *sc_des1_inst){
	Xil_Out32(CONTACTOR_DISENGAGE_ADDR, contactor_setting.reg);
	sc_des1_inst->alarm_data->contactor_disengage_reg.reg = contactor_setting.reg;
}

void set_system_frequency(int freq, SC_DES1_T *sc_des1_inst) {
	sc_des1_inst->alarm_data->control_reg.bits.freq60_50 = freq;
	set_control_reg(sc_des1_inst, &sc_des1_inst->alarm_data->control_reg);
}

